package org.gmwframework.cloud.mybatis.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.gmwframework.cloud.common.base.PageableQuery;
import org.gmwframework.cloud.common.base.PageableResult;
import org.gmwframework.cloud.mybatis.convert.PageMapStruct;

/**
 *
 */
@SuppressWarnings("unchecked")
public class PageHelper {

    /**
     * 分页
     *
     * @param query   请求参数
     * @param service 接口
     * @return 结果
     */
    public static <T, V extends PageableQuery> PageableResult<T> page(V query, MybatisPlusPageQueryService<T, V> service) {
        IPage<T> tiPage = new Page<>();
        tiPage.setSize(query.getSize());
        tiPage.setCurrent(query.getCurrent());
        tiPage = service.data((Page<T>) tiPage, query);
        return (PageableResult<T>) PageMapStruct.getInstance().plusPage2Local((Page<T>) tiPage);
    }


    /**
     * 分页
     *
     */
    public static <T, V extends PageableQuery> PageableResult<T> page(V query, BaseMapper<T> baseMapper, Wrapper<T> queryWrapper) {
        Page page = PageMapStruct.getInstance().localPagination2PlusPage(query);
        page.setSize(query.getSize());
        page.setCurrent(query.getCurrent());
        page = (Page) baseMapper.selectPage(page, queryWrapper);
        return (PageableResult<T>) PageMapStruct.getInstance().plusPage2Local(page);
    }

}

