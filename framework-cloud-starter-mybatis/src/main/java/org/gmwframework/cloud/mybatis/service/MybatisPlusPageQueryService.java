package org.gmwframework.cloud.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.gmwframework.cloud.common.base.PageableQuery;


@FunctionalInterface
public interface MybatisPlusPageQueryService<T, V extends PageableQuery> {

    IPage<T> data(Page<T> page, V query);

}
