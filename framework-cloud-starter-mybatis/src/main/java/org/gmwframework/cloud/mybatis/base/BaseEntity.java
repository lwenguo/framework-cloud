package org.gmwframework.cloud.mybatis.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.gmwframework.cloud.common.base.BaseModel;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author guomw
 * @date 2022/10/08 19:36
 * @since 1.0.0
 */
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class BaseEntity extends BaseModel implements Serializable {
    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 乐观锁
     */
    @Version
    private Integer version;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", update = "now()", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
