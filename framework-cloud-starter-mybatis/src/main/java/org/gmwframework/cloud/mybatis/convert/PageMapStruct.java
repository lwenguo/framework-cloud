package org.gmwframework.cloud.mybatis.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.gmwframework.cloud.common.base.PageableQuery;
import org.gmwframework.cloud.common.base.PageableResult;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/18 4:13 下午
 */
@SuppressWarnings(value = {"rawtypes", "unchecked"})
@Mapper
public interface PageMapStruct {
    static PageMapStruct getInstance() {
        return PageMapStructInstance.INSTANCE;
    }

    final class PageMapStructInstance {
        private static final PageMapStruct INSTANCE = Mappers.getMapper(PageMapStruct.class);

        private PageMapStructInstance() {
        }
    }

    @Mappings({
            @Mapping(source = "current", target = "pageIndex"),
            @Mapping(source = "total", target = "total"),
            @Mapping(source = "size", target = "pageSize"),
            @Mapping(source = "records", target = "list")

    })
    PageableResult plusPage2Local(Page page);

    @Mappings({
            @Mapping(source = "pageIndex", target = "current"),
            @Mapping(source = "pageSize", target = "size")
    })
    Page localPagination2PlusPage(PageableQuery pageQuery);
}
