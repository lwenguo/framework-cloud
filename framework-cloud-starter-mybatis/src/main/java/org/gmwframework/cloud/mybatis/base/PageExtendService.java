package org.gmwframework.cloud.mybatis.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.gmwframework.cloud.common.base.PageableQuery;
import org.gmwframework.cloud.common.base.PageableResult;


public interface PageExtendService<T> extends IService<T> {

    /**
     * @param query        分页请求
     * @param queryWrapper 条件,可以为空
     * @return 分页结果
     */
    default PageableResult<T> page(PageableQuery query, Wrapper<T> queryWrapper) {
        IPage<T> tiPage = new Page<>();
        tiPage.setSize(query.getSize());
        tiPage.setCurrent(query.getCurrent());
        IPage<T> result = page(tiPage, queryWrapper != null ? queryWrapper : Wrappers.emptyWrapper());
        return PageableResult.<T>builder()
                .records(result.getRecords())
                .size(result.getSize())
                .pages(result.getPages())
                .current(result.getCurrent())
                .total(result.getTotal())
                .build();
    }
}
