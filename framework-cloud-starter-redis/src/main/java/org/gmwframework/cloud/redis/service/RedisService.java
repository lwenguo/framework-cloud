package org.gmwframework.cloud.redis.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Transaction;

import javax.annotation.Resource;
import javax.security.auth.kerberos.KerberosTicket;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * spring redis 工具类
 *
 * @author ruoyi
 **/
@SuppressWarnings(value = {"unchecked", "rawtypes"})
@Configuration
public class RedisService {
    @Resource
    public RedisTemplate redisTemplate;

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key   缓存的键值
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key      缓存的键值
     * @param value    缓存的值
     * @param timeout  时间
     * @param timeUnit 时间颗粒度
     */
    public <T> void setCacheObject(final String key, final T value, final Long timeout, final TimeUnit timeUnit) {
        setCacheObject(key, value);
        expire(key, timeout, timeUnit);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(final String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @param unit    时间单位
     * @return true=设置成功；false=设置失败
     */
    public boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    public boolean hasKey(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final String key) {
        ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key);
    }

    /**
     * 删除单个对象
     *
     */
    public Boolean deleteObject(final String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 删除对象
     *
     */
    public Boolean deleteObject(final String key, final String hKey) {
        Long result = redisTemplate.opsForHash().delete(key, hKey);
        return result == 1;
    }

    /**
     * 删除集合对象
     *
     * @param collection 多个对象
     */
    public Long deleteObject(final Collection collection) {
        return redisTemplate.delete(collection);
    }

    /**
     * 缓存List数据 添加
     *
     * @param key      缓存的键值
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    public <T> long setCacheList(final String key, final List<T> dataList) {
        Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
        return count == null ? 0 : count;
    }

    public <T> long setCacheList(final String key, final T data) {
        Long count = redisTemplate.opsForList().rightPush(key, data);
        return count == null ? 0 : count;
    }


    public <T> long deleteList(final String key, final int count, final T data) {
        Long num = redisTemplate.opsForList().remove(key, count, data);
        return num == null ? 0 : num;
    }

    /**
     * 删除list指定元素
     *
     */
    public <T> long deleteList(final String key, final T data) {
        return deleteList(key, 1, data);
    }

    /**
     * 缓存List数据 覆盖
     *
     */
    public <T> long putCacheList(final String key, final List<T> dataList) {
        deleteObject(key);
        return setCacheList(key, dataList);
    }


    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(final String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    /**
     * 获得缓存的list对象
     *
     * @param key   缓存的键值
     * @param start 获取区间范围-开始
     * @param end   获取区间范围-结束
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(final String key, long start, long end) {
        return redisTemplate.opsForList().range(key, start, end);
    }


    /**
     * 缓存Set
     *
     * @param key     缓存键值
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> long setCacheSet(final String key, final Set<T> dataSet) {
        Long count = redisTemplate.opsForSet().add(key, dataSet);
        return count == null ? 0 : count;
    }

    /**
     * 获得缓存的set
     *
     */
    public <T> Set<T> getCacheSet(final String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 缓存Map
     *
     */
    public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
        if (dataMap != null) {
            redisTemplate.opsForHash().putAll(key, dataMap);
        }
    }

    /**
     * 获得缓存的Map
     *
     */
    public <T> Map<String, T> getCacheMap(final String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 往Hash中存入数据
     *
     * @param key   Redis键
     * @param hKey  Hash键
     * @param value 值
     */
    public <T> void setCacheMapValue(final String key, final String hKey, final T value) {
        redisTemplate.opsForHash().put(key, hKey, value);
    }

    /**
     * 获取Hash中的数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    public <T> T getCacheMapValue(final String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
        if (opsForHash.hasKey(key, hKey)) {
            return opsForHash.get(key, hKey);
        }
        return null;
    }

    /**
     * 判断是否存在
     *
     */
    public boolean hasKey(final String key, final String hKey) {
        return redisTemplate.opsForHash().hasKey(key, hKey);
    }

    /**
     * 获取多个Hash中的数据
     *
     * @param key   Redis键
     * @param hKeys Hash键集合
     * @return Hash对象集合
     */
    public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hKeys) {
        return redisTemplate.opsForHash().multiGet(key, hKeys);
    }

    /**
     * 获取hash key
     *
     */
    public <T> Set<T> getHashKeys(final String key) {
        return redisTemplate.opsForHash().keys(key);
    }

    /**
     * 获得缓存的基本对象列表
     *
     * @param pattern 字符串前缀
     * @return 对象列表
     */
    public Collection<String> keys(final String pattern) {
        return redisTemplate.keys(pattern);
    }


    public Long decrement(final String key, Long step) {
        return redisTemplate.opsForValue().decrement(key, step);
    }

    /**
     * 初始化监控数量
     *
     */
    public void initWatchCount(String key, int maxCount) {
        deleteObject(key);
        RedisOperations redisOperations = redisTemplate.opsForHash().getOperations();
        redisOperations.watch(key);
        ZSetOperations<String, String> zSet = redisOperations.opsForZSet();
        Double aDouble = zSet.score(key, key);
        if (aDouble == null) {
            zSet.incrementScore(key, key, maxCount);
        }
    }

    /**
     * 监听
     *
     * @param key      redisKey
     * @param runnable 执行函数
     */
    public boolean watch(String key, Runnable runnable) {
        List<Object> results = (List<Object>) redisTemplate.execute(new SessionCallback() {
            @Override
            public List<Object> execute(RedisOperations operations) throws DataAccessException {
                operations.watch(key);
                ZSetOperations<String, String> kvzSetOperations = operations.opsForZSet();
                Double score = kvzSetOperations.score(key, key);
                operations.multi();
                if (score != null && score > 0) {
                    kvzSetOperations.incrementScore(key, key, -1);
                    runnable.run();
                }
                return operations.exec();
            }
        });
        return results != null && results.size() > 0;
    }
}
