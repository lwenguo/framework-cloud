package org.gmwframework.cloud.redis.annotation;


import org.gmwframework.cloud.redis.base.LockType;

import java.lang.annotation.*;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/20 3:27 下午
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {
    /**
     * 锁key
     */
    String lockKey();

    /**
     * 服务ID
     *
     */
    String serviceId() default "";

    /**
     * 锁类型
     *
     */
    LockType lockType() default LockType.BLOCK;

    /**
     * 超时时间 秒 ,默认120秒
     *
     */
    long timeout() default 300;

    /**
     * 上锁后自动解锁时间 秒 ,默认15秒
     *
     */
    long unlockTimeout() default 15;
}
