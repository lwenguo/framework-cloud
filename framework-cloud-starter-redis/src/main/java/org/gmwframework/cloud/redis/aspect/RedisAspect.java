package org.gmwframework.cloud.redis.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.gmwframework.cloud.redis.RedisLocker;
import org.gmwframework.cloud.redis.annotation.RedisLock;
import org.gmwframework.cloud.redis.base.LockType;
import org.redisson.client.RedisException;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;


/**
 * redis 分布式锁
 * 使用方式：在方法头，添加注解@RedisLock
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/20 3:29 下午
 */
@Configuration
@Aspect
@Slf4j
public class RedisAspect {
    @Resource
    private RedisLocker redisLocker;

    /**
     * 声明该方法为一个前置通知:在目标方法开始之前执行
     *
     */
    @Before("@annotation(redisLock)")
    private void beforeMethod(JoinPoint joinPoint, RedisLock redisLock) {
        LockType lockType = redisLock.lockType();
        String lockKey = getLockKey(redisLock);
        if (lockType == LockType.BLOCK) {
            while (true) {
                //尝试获取锁 最多等待timeout秒，超出不等，上锁以后unlockTimeout秒自动解锁
                boolean locked = redisLocker.tryLock(lockKey, redisLock.timeout(), redisLock.unlockTimeout());
                if (locked) {
                    break;
                }
            }
        } else {
            //尝试获取锁
            boolean locked = redisLocker.tryLock(lockKey, redisLock.timeout(), redisLock.unlockTimeout());
            if (!locked) {
                throw new RedisException("获取锁失败");
            }
        }
    }

    /**
     * 后置通知，就是在目标方法执行之后（无论是否发生异常）执行的通知
     * 后置通知中不能访问目标方法的返回结果
     *
     */
    @After("@annotation(redisLock)")
    public void afterMethod(JoinPoint joinPoint, RedisLock redisLock) {
        redisLocker.release(getLockKey(redisLock));
    }

    private String getLockKey(RedisLock redisLock) {
        return "redis:son:lock:" + redisLock.lockKey() + redisLock.serviceId();
    }
}
