package org.gmwframework.cloud.redis;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/20 3:24 下午
 */
@Configuration
@Slf4j
public class RedisLocker {

    @Resource
    private RedissonClient redissonClient;

    /**
     * @param lockKey       锁
     * @param timeout       尝试加锁，最多等待时间 秒
     * @param unlockTimeout 上锁后自动解锁时间 秒
     */
    public boolean tryLock(String lockKey, long timeout, long unlockTimeout) {
        RLock lock = redissonClient.getFairLock(lockKey);
        try {
            return lock != null && lock.tryLock(timeout, unlockTimeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            return false;
        }
    }

    public void release(String lockKey) {
        RLock lock = redissonClient.getFairLock(lockKey);
        if (lock.isLocked()) {
            lock.unlock();
        }
    }

}
