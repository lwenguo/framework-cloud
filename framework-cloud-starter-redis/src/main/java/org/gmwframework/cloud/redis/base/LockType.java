package org.gmwframework.cloud.redis.base;


public enum LockType {
    BLOCK,
    UNBLOCK;
}
