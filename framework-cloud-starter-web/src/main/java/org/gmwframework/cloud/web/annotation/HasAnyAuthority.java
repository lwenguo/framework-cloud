package org.gmwframework.cloud.web.annotation;

import java.lang.annotation.*;


@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface HasAnyAuthority {
    String[] value() default {};
}
