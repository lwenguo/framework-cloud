package org.gmwframework.cloud.web.advice;

import com.alibaba.fastjson.JSON;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.gmwframework.cloud.common.base.ApiResult;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.constant.FeignHttpHeaders;
import org.gmwframework.cloud.web.annotation.ResponseResult;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Objects;

/**
 * @author lanxi
 * @date 2020/10/10
 */
@Slf4j
@RestControllerAdvice(annotations = ResponseResult.class)
public class ResponseControllerAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(@NonNull MethodParameter returnType,
                            @NonNull Class<? extends HttpMessageConverter<?>> aClass) {
        return !returnType.getParameterType().equals(ApiResult.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, @NonNull MethodParameter returnType,
                                  @NonNull MediaType mediaType, @NonNull Class<? extends HttpMessageConverter<?>> aClass,
                                  @NonNull ServerHttpRequest serverHttpRequest, @NonNull ServerHttpResponse serverHttpResponse) {

        // 对于Feign调用的请求直接返回结果
        if (serverHttpRequest.getHeaders().containsKey(FeignHttpHeaders.FEIGN)) {
            return body;
        }
        ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) serverHttpRequest;
        String xTraceId = servletRequest.getServletRequest().getAttribute(FeignHttpHeaders.X_TRACE_ID).toString();
        Object obj;
        if (body instanceof ApiResult) {
            obj = body;
        } else if (body instanceof Exception) {
            obj = ApiResult.error(CommonCode.FAIL_SERVER, ((Exception) body).getMessage(), xTraceId);
        } else if (body instanceof Boolean) {
            obj = ApiResult.success(((Boolean) body).toString(), xTraceId);
        } else if (body instanceof String) {
            obj = JSON.toJSONString(ApiResult.success(body, xTraceId));
        } else {
            obj = ApiResult.success(body, xTraceId);
        }

        if (log.isDebugEnabled()) {
            String methodName = Objects.requireNonNull(returnType.getMethod()).getName();
            log.debug("{} return = {}", methodName, (obj instanceof String) ? obj : JSON.toJSONString(obj));
        }
        return obj;
    }

    @InitBinder
    public void binder(WebDataBinder webDataBinder) {
        StringTrimmerEditor propertyEditor = new StringTrimmerEditor(false);
        webDataBinder.registerCustomEditor(String.class, propertyEditor);
    }
}
