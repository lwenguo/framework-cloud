package org.gmwframework.cloud.web.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 跨越配置
 *
 * @author guomw
 * @date 2022/11/08 15:12
 * @since 1.0.0
 */
@Data
@Configuration
public class CorsProperties {

    @Value("${gmwframework.cors.enable:true}")
    private boolean enable;

    @Value("${gmwframework.cors.allowed_origins:*}")
    private List<String> allowedOrigins;

    @Value("${gmwframework.cors.allowed_headers:*}")
    private List<String> allowedHeaders;
}
