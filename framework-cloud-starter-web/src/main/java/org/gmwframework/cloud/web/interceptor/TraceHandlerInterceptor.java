package org.gmwframework.cloud.web.interceptor;

import cn.hutool.core.util.StrUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.gmwframework.cloud.common.constant.FeignHttpHeaders;
import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * 处理前检查是否有TraceId<br>
 * 如果没有则设置一个
 *
 * @author wangchen
 * @version 1.0
 * @date 2022/4/9 2022
 */
@Slf4j
public class TraceHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                             @NonNull Object handler) throws Exception {
        String xTraceId = request.getHeader(FeignHttpHeaders.X_TRACE_ID);
        String feign = request.getHeader(FeignHttpHeaders.FEIGN);

        MDC.clear();
        if (StrUtil.isNotBlank(xTraceId)) {
            MDC.put(FeignHttpHeaders.X_TRACE_ID, xTraceId);
        } else {
            String uuid = UUID.randomUUID().toString();
            MDC.put(FeignHttpHeaders.X_TRACE_ID, uuid);
            xTraceId = uuid;
        }
        request.setAttribute(FeignHttpHeaders.X_TRACE_ID, xTraceId);
        if (StrUtil.isNotBlank(feign)) {
            // 放入Feign标记
            MDC.put(FeignHttpHeaders.FEIGN, feign);
        }

        if (log.isDebugEnabled()) {
            log.debug("request请求地址 path[{}] uri[{}]", request.getServletPath(), request.getRequestURI());
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                           @NonNull Object handler, ModelAndView modelAndView) throws Exception {
        MDC.remove(FeignHttpHeaders.X_TRACE_ID);
        MDC.remove(FeignHttpHeaders.FEIGN);
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }
}
