/**
 * @(#)MsgHelper.java, 2023/6/17.
 * <p/>
 * Copyright 2023 Pinheng, Inc. All rights reserved.
 * Pinheng PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.gmwframework.cloud.web.helper;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.common.async.AsyncService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wangchen
 * @version 1.0
 * @since 2023/6/17 2023
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class FsNotifyHelper {

    private static final String REGION = "msg-notify";

    private final StringRedisTemplate redisTemplate;

    private final ObjectMapper objectMapper;

    private final AsyncService asyncService;

    /**
     * 发送飞书通知消息
     *
     * @param msgId     消息ID<br>
     *                  如果只被调用发单次消息，id为 null<br>
     *                  如果会被重复调用发消息, id为唯一值，供缓存阻挡过滤
     * @param content   消息内容体，有些需要符合飞书语法
     * @param notifyUrl 发送目的地，飞书消息群地址
     * @param retry     true 消息重复发送
     */
    public void sendNotify(String msgId, String content, String notifyUrl, boolean retry) {
        asyncService.executeAsync(() -> asyncSendNotify(msgId, content, notifyUrl, retry, false));
    }

    /**
     * 发送飞书通知信息 - 卡片模式
     *
     * @param msgId     消息ID<br>
     *                  如果只被调用发单次消息，id为 null<br>
     *                  如果会被重复调用发消息, id为唯一值，供缓存阻挡过滤
     * @param title     消息标题
     * @param data      消息内容体，有些需要符合飞书语法
     * @param notifyUrl 发送目的地，飞书消息群地址
     * @param atOpenId  飞书用户OpenId，all 表示 at 所有群用户，为空表示不 at 任何用户
     * @param retry     true 消息重复发送
     */
    public void sendNotify(String msgId, String title, LinkedHashMap<String, String> data, String notifyUrl,
                           String atOpenId, boolean retry) {
        if (StringUtils.isBlank(notifyUrl)) {
            return;
        }
        asyncService.executeAsync(() -> {
            List<List<ZhContentBean>> content = new ArrayList<>();
            for (Map.Entry<String, String> entry : data.entrySet()) {
                List<ZhContentBean> list = new ArrayList<>();
                if (StringUtils.isNotBlank(entry.getKey())) {
                    list.add(ZhContentBean.builder().tag("text").text(entry.getKey() + "：").build());
                }
                list.add(ZhContentBean.builder().tag("text").text(entry.getValue()).build());
                content.add(list);
            }
            if (StringUtils.isNotBlank(atOpenId)) {
                List<ZhContentBean> list = new ArrayList<>();
                list.add(ZhContentBean.builder().tag("at").userId(atOpenId).build());
                content.add(list);
            }
            Map<String, Object> zhBean = new LinkedHashMap<>();
            zhBean.put("title", title);
            zhBean.put("content", content);

            Map<String, Object> postBean = new LinkedHashMap<>();
            postBean.put("zh_cn", zhBean);

            Map<String, Object> contentBean = new LinkedHashMap<>();
            contentBean.put("post", postBean);

            Map<String, Object> msgBean = new LinkedHashMap<>();
            msgBean.put("msg_type", "post");
            msgBean.put("content", contentBean);
            asyncSendNotify(msgId, msgBean, notifyUrl, retry, true);
        });
    }

    private void asyncSendNotify(String msgId, Object content, String notifyUrl, boolean retry, boolean richText) {
        if (StringUtils.isEmpty(notifyUrl)) {
            return;
        }
        if (retry && StrUtil.isNotBlank(msgId) && Boolean.TRUE.equals(redisTemplate.hasKey(REGION + msgId))) {
            return;
        } else if (!retry && StrUtil.isNotBlank(msgId)) {
            redisTemplate.delete(REGION + msgId);
        }
        log.info(content.toString());
        Object msgBean;
        if (!richText) {
            msgBean = MsgBean.builder().content(MsgBean.ContentBean.builder().text(content.toString()).build()).build();
        } else {
            msgBean = content;
        }
        String jsonContent;
        try {
            jsonContent = objectMapper.writeValueAsString(msgBean);
        } catch (JsonProcessingException e) {
            return;
        }

        HttpRequest request = HttpRequest.post(notifyUrl);
        request.contentType("application/json;charset=UTF-8").body(jsonContent);
        try (HttpResponse response = request.execute()) {
            if (response.isOk()) {
                MsgResult result = objectMapper.readValue(response.body(), MsgResult.class);
                if (!result.code.equals("0")) {
                    log.error("消息发送失败: {}", result.getMsg());
                } else {
                    if (retry && StrUtil.isNotBlank(msgId)) {
                        // 设置消息已经发送的占位
                        redisTemplate.opsForValue().set(REGION + msgId, "1");
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Data
    @Builder
    private static class MsgBean {

        @JsonProperty(value = "msg_type")
        private final String msgType = "text";

        private ContentBean content;

        @Data
        @Builder
        private static class ContentBean {

            private String text;
        }
    }

    @Data
    @NoArgsConstructor
    private static class MsgResult {

        @JsonProperty(value = "StatusCode")
        private String statusCode;

        @JsonProperty(value = "StatusMessage")
        private String statusMessage;

        private String code;

        private String msg;

        private Object data;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class ZhContentBean {
        @Builder.Default
        private String tag = "text";

        private String text;

        @Builder.Default
        @JsonProperty("user_id")
        private String userId = "all";
    }
}
