package org.gmwframework.cloud.web.interceptor;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.common.base.ApiResult;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.exception.ApiException;
import org.gmwframework.cloud.web.annotation.HasAnyAuthority;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户拦截器
 *
 * @author allan
 * @version 1.0.0
 * @date 2022/4/15
 */
public class GlobalAuthInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        String userStr = Base64.decodeStr(request.getHeader(FeignHttpHeaders.HEADER_USER));
//
//        if (StringUtils.isNotEmpty(userStr)) {
//            LoginUser loginUser = JSON.parseObject(userStr, LoginUser.class);
//
//            YcAuthContextHolder.setContext(loginUser);
//            request.setAttribute(AuthConstant.LOGIN_USER, loginUser);
//
//            if (loginUser.getSysClient().equals(SysClientEnum.CHN.getCode())) {
//                if (ObjectUtil.equals(loginUser.getRole(), AuthConstant.ROLE_ADMIN)) {
//                    return true;
//                }
//                //校验权限
//                HandlerMethod handlerMethod = (HandlerMethod) handler;
//                HasAnyAuthority anyAuthority = handlerMethod.getBeanType().getAnnotation(HasAnyAuthority.class);
//                if (anyAuthority == null) {
//                    anyAuthority = handlerMethod.getMethodAnnotation(HasAnyAuthority.class);
//                }
//
//                if (anyAuthority == null) {
//                    return true;
//                } else {
//                    String[] value = anyAuthority.value();
//                    if (ArrayUtil.contains(value, loginUser.getRole())) {
//                        return true;
//                    } else {
//                        throw new ApiException(ApiResult.error(CommonCode.UNAUTHORIZED));
//                    }
//                }
//            }
//        }

        return true;
    }
}
