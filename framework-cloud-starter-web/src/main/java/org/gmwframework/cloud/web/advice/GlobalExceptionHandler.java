package org.gmwframework.cloud.web.advice;

import lombok.extern.slf4j.Slf4j;
import org.gmwframework.cloud.common.base.ApiResult;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.constant.FeignHttpHeaders;
import org.gmwframework.cloud.common.exception.ApiException;
import org.gmwframework.cloud.web.helper.FsNotifyHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 全局异常处理
 *
 * @author wangchen
 * @date 2020/8/19
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @Resource
    private FsNotifyHelper fsNotifyHelper;

    @Value("${api.webhook:}")
    private String webHook;

    @ResponseBody
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ApiResult<Void> processRequestParameterException(HttpServletRequest request, HttpServletResponse response,
                                                            MissingServletRequestParameterException e) {
        log.error("MissingServletRequestParameterException ", e);
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.INVALID_PARAMS_FORMAT, "Missing servlet request parameter");
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());

        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ApiResult<Void> processDefaultException(HttpServletRequest request, HttpServletResponse response, Exception e) {
        log.error("Exception ", e);
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.ILLEGAL_PARAMS, "Exception");
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }


    @ResponseBody
    @ExceptionHandler(value = AccessDeniedException.class)
    public ApiResult<Void> processDefaultException(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) {
        log.error("AccessDeniedException ", e);
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.ACCESS_DENIED);
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }


    @ResponseBody
    @ExceptionHandler(value = DataAccessException.class)
    public ApiResult<Void> processDataAccessException(HttpServletRequest request, HttpServletResponse response, DataAccessException e) {
        log.error("DataAccessException ", e);
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.ILLEGAL_PARAMS, "Data access exception");
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }

    @ResponseBody
    @ExceptionHandler(value = ApiException.class)
    public ApiResult<Void> processBizException(HttpServletRequest request, ApiException e) {
        log.error("ApiException ", e);
        ApiResult<Void> apiResult = e.getApiResult();
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }


    @ResponseBody
    @ExceptionHandler(value = {BindException.class})
    public ApiResult<Void> handleValidException(HttpServletRequest request, HttpServletResponse response, BindException e) {
        log.error("BindException ", e);
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.ILLEGAL_PARAMS, "Method argument not valid or bind exception");
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }


    @ResponseBody
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ApiResult<Void> handleMethodArgumentNotValidException(HttpServletRequest request, HttpServletResponse response, MethodArgumentNotValidException e) {
        log.error("MethodArgumentNotValidException " + e.getMessage(), e);
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        String message = "Method argument not valid or bind exception";
        if (allErrors.size() > 0) {
            message = allErrors.get(0).getDefaultMessage();
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.PARAMETER_ERROR, message);
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }


    @ResponseBody
    @ExceptionHandler(value = IllegalArgumentException.class)
    public ApiResult<Void> handleIllegalArgumentException(HttpServletRequest request, HttpServletResponse response, IllegalArgumentException e) {
        log.error("IllegalArgumentException ", e);
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.ILLEGAL_PARAMS, e.getMessage());
        apiResult.setXTraceId(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString());
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }

    @ResponseBody
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ApiResult<Void> handleHttpMessageNotReadableException(HttpServletRequest request, HttpServletResponse response,
                                                                 HttpMessageNotReadableException e) {
        log.error("HttpMessageNotReadableException ", e);
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setContentType("application/json;charset=UTF-8");
        ApiResult<Void> apiResult = ApiResult.error(CommonCode.ILLEGAL_PARAMS, "JSON parse error");
        apiResult.setXTraceId(request.getHeader(FeignHttpHeaders.X_TRACE_ID));
        fsNotifyHelper.sendNotify(request.getAttribute(FeignHttpHeaders.X_TRACE_ID).toString(), e.getMessage(), webHook, false);
        return apiResult;
    }
}
