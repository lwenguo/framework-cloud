package org.gmwframework.cloud.auth.config;

import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.auth.base.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author guomw
 * @date 2022/11/05 18:59
 * @since 1.0.0
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Value("${gmwframework.oauth.matchers:}")
    private List<String> matchers;

    /**
     * 忽略验证权限
     */
    private static final String[] PERMIT_ALL_ANT_MATCHERS = {
            "/init",
            "/user/register",
            "/doc.html",
            "/webjars/**",
            "/swagger-resources",
            "/i/health",
            "/v2/api-docs",
            "/oauth/token"
    };

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        RestAuthenticationEntryPoint restAuthenticationEntryPoint = new RestAuthenticationEntryPoint();
        resources.authenticationEntryPoint(restAuthenticationEntryPoint)
                .resourceId("gmwframework")
                .stateless(true);//配置资源id，与授权服务器配置的资源id一致，基于令牌认证
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        if (matchers == null) {
            matchers = new ArrayList<>();
        }
        matchers.addAll(Arrays.asList(PERMIT_ALL_ANT_MATCHERS));
        http.authorizeRequests()
                .antMatchers(matchers.toArray(new String[0])).permitAll()
                .antMatchers().permitAll()
                .anyRequest().authenticated();
    }
}
