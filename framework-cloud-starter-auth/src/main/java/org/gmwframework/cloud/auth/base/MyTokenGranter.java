package org.gmwframework.cloud.auth.base;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author allan
 * @version 1.0.0
 * @date 2022/4/7
 */
public class MyTokenGranter extends AbstractTokenGranter {

    private final AuthenticationManager authenticationManager;

    public MyTokenGranter(String grantType, AuthenticationManager authenticationManager,
                          AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        this(authenticationManager, tokenServices, clientDetailsService, requestFactory, grantType);
    }

    public MyTokenGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices,
                          ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory, String grantType) {
        super(tokenServices, clientDetailsService, requestFactory, grantType);
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());

        String username = parameters.get("username");
        String password = parameters.get("password");
        String sysClient = parameters.get("sysClient");
        parameters.remove("password");

        Authentication userAuth = new MyAuthenticationToken(username, password, sysClient);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);

        userAuth = authenticationManager.authenticate(userAuth);

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);
    }
}
