package org.gmwframework.cloud.auth.base;

/**
 * @author guomw
 * @date 2022/11/05 14:50
 * @since 1.0.0
 */
public interface UserDetails extends org.springframework.security.core.userdetails.UserDetails {

    Object getId();

    String getUserNo();

    String getSysClient();

    String getName();

    String getMobile();

    Long getOrgId();

    Long getRootOrgId();

    String getRoleIds();

    boolean isSysUser();

    Integer getUserType();

    String getAvatar();
}
