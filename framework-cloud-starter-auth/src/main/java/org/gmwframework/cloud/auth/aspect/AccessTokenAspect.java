package org.gmwframework.cloud.auth.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;


/**
 * 令牌校验 作废
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/5/8 4:31 下午
 */
@Aspect
@Configuration
public class AccessTokenAspect {

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    private void accessTokenAspectPointCut() {
        //
    }

    /**
     * 检查用户是否已登录
     */
    @Around("accessTokenAspectPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        return point.proceed();
    }
}
