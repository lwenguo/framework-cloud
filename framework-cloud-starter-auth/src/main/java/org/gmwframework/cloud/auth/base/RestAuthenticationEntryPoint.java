package org.gmwframework.cloud.auth.base;

import com.alibaba.fastjson.JSON;
import org.gmwframework.cloud.common.base.ApiResult;
import org.gmwframework.cloud.common.base.CommonCode;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义返回结果：没有登录或token过期时
 * Created by macro on 2020/6/18.
 */
@Component
public class RestAuthenticationEntryPoint extends OAuth2AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        //如果是client_id和client_secret相关异常 返回自定义的数据格式
        if (e instanceof BadCredentialsException) {
            response.setStatus(HttpStatus.OK.value());
            response.setHeader("Content-Type", "application/json;charset=UTF-8");
            String body = JSON.toJSONString(ApiResult.error(CommonCode.UNAUTHORIZED, "invalid_client"));
            response.getWriter().write(body);
        } else if (e instanceof InsufficientAuthenticationException) {
            response.setStatus(HttpStatus.OK.value());
            response.setHeader("Content-Type", "application/json;charset=UTF-8");
            String body = JSON.toJSONString(ApiResult.error(CommonCode.UNAUTHORIZED));
            response.getWriter().write(body);
        } else {
            super.commence(request, response, e);
        }
    }
}
