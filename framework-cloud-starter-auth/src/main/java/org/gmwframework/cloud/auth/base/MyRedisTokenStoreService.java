package org.gmwframework.cloud.auth.base;

import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import java.util.Date;

/**
 * 实现自动更新access_token
 *
 * @author guomw
 * @date 2022/11/07 16:45
 * @since 1.0.0
 */
public class MyRedisTokenStoreService extends RedisTokenStore {
    private final ClientDetailsService clientDetailsService;

    public MyRedisTokenStoreService(RedisConnectionFactory connectionFactory, ClientDetailsService clientDetailsService) {
        super(connectionFactory);
        this.clientDetailsService = clientDetailsService;
    }

    @Override
    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
        OAuth2Authentication result = readAuthentication(token.getValue());
        if (result != null) {
            // 如果token没有失效  更新AccessToken过期时间
            int validitySeconds = getAccessTokenValiditySeconds(result.getOAuth2Request());
            //重新设置过期时间
            if (validitySeconds <= 3600) {
                DefaultOAuth2AccessToken oAuth2AccessToken = (DefaultOAuth2AccessToken) token;
                oAuth2AccessToken.setExpiration(new Date(System.currentTimeMillis() + (60 * 60 * 72 * 1000L)));
                //将重新设置过的过期时间重新存入redis, 此时会覆盖redis中原本的过期时间
                storeAccessToken(token, result);
            }
        }
        return result;
    }

    protected int getAccessTokenValiditySeconds(OAuth2Request clientAuth) {
        if (clientDetailsService != null) {
            ClientDetails client = clientDetailsService.loadClientByClientId(clientAuth.getClientId());
            Integer validity = client.getAccessTokenValiditySeconds();
            if (validity != null) {
                return validity;
            }
        }
        return -1;
//        // default 4 hours.
//        int accessTokenValiditySeconds = 60 * 60 * 24*30;
//        return accessTokenValiditySeconds;
    }


}
