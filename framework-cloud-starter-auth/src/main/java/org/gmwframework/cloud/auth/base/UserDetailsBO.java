package org.gmwframework.cloud.auth.base;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;


@Data
@NoArgsConstructor
public class UserDetailsBO implements UserDetails {
    private static final long serialVersionUID = -6572672817405799276L;
    private Long id;

    private String userNo;

    /**
     * 所属系统
     */
    private String sysClient;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;
    /**
     * 所属组织架构id
     */
    private Long orgId;
    /**
     * 所属母组织架构id
     */
    private Long rootOrgId;
    /**
     * 名称
     */
    private String name;

    private String mobile;

    /**
     * 角色id
     */
    private String roleIds;

    /**
     * 是否可用
     */
    private boolean disabled;

    private LocalDateTime createTime;
    /**
     * 系统用户
     */
    private boolean sysUser;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 头像
     */
    private String avatar;


    private Set<String> permissions;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (sysUser) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else {
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        if(permissions!=null) {
            for (String permission : permissions) {
                authorities.add(new SimpleGrantedAuthority(permission));
            }
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !isDisabled();
    }

}
