package org.gmwframework.cloud.auth.base;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@Setter
@Getter
public class MyAuthenticationToken extends UsernamePasswordAuthenticationToken {
    private final String client;

    public MyAuthenticationToken(Object principal, Object credentials, String client) {
        super(principal, credentials);
        this.client = client;
    }
}
