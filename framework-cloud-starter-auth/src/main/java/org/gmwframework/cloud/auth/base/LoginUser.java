package org.gmwframework.cloud.auth.base;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guomw 2022/11/05 21:04
 * @since 1.0.0
 */
@Data
public class LoginUser {
    /**
     *
     */
    private Long userId;

    private String userNo;

    private String avatar;

    /**
     * 所属系统
     */
    private String sysClient;

    /**
     * 用户名
     */
    private String username;

    /**
     * 所属组织架构id
     */
    private Long orgId;
    /**
     * 所属母组织架构id
     */
    private Long rootOrgId;
    /**
     * 名称
     */
    private String name;

    private String mobile;
    /**
     * 角色id
     */
    private String roleIds;

    /**
     * 是否可用
     */
    private boolean disabled;
    /**
     * 是否是系统用户
     */
    private boolean sysUser;
    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 是否存在根级ID
     *
     * @return 存在或不存在
     */
    public boolean isRootOrgId() {
        return rootOrgId != null && rootOrgId > 0;
    }


    public List<Long> getRoleIdList() {
        List<Long> list = new ArrayList<>();
        if (StringUtils.isEmpty(roleIds)) {
            return null;
        }
        String[] arr = this.roleIds.split(",");
        for (String val : arr) {
            if (StringUtils.isEmpty(val)) {
                continue;
            }
            list.add(Long.valueOf(val));
        }
        return list;
    }


}
