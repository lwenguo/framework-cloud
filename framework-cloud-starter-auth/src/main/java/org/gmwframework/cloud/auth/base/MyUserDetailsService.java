package org.gmwframework.cloud.auth.base;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author guomw
 * @date 2022/11/07 17:58
 * @since 1.0.0
 */
public interface MyUserDetailsService extends UserDetailsService {

    /**
     * 根据用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户信息
     * @throws UsernameNotFoundException 用户名不存在异常
     */
    UserDetailsBO loadUserByUsername(String username) throws UsernameNotFoundException;
}
