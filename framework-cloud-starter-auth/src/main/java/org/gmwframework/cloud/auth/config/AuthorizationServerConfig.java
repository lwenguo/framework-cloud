package org.gmwframework.cloud.auth.config;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.auth.base.*;
import org.gmwframework.cloud.auth.base.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;

import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableAuthorizationServer
@RequiredArgsConstructor
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    private final AuthenticationManager authenticationManager;
    private final MyUserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final MyRedisTokenStoreService myRedisTokenStoreService;
    private final ClientDetailsService myClientDetailsService;

    @Value("${gmwframework.oauth.access_token_expire:14400}")
    private int accessTokenExpire;
    @Value("${gmwframework.oauth.refresh_token_expire:21600}")
    private int refreshTokenExpire;
    @Value("${gmwframework.oauth.client_id:client}")
    private String clientId;
    @Value("${gmwframework.oauth.grant_type:password}")
    private String grantType;
    @Value("${gmwframework.oauth.client_secret:}")
    private String clientSecret;

    @Value("${gmwframework.oauth.enabled_jdbc:false}")
    private boolean enabledJdbc;


    /**
     * 创建grant_type列表
     */
    private TokenGranter tokenGranter(AuthorizationServerEndpointsConfigurer endpoints) {
        List<TokenGranter> list = new ArrayList<>();
        // 这里配置密码模式
//        if (authenticationManager != null) {
//            list.add(new ResourceOwnerPasswordTokenGranter(authenticationManager,
//                    endpoints.getTokenServices(),
//                    endpoints.getClientDetailsService(),
//                    endpoints.getOAuth2RequestFactory()));
//        }
        //刷新token模式、
        MyTokenServices tokenServices = new MyTokenServices();
        tokenServices.setTokenStore(endpoints.getTokenStore());
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setReuseRefreshToken(true);
        tokenServices.setClientDetailsService(endpoints.getClientDetailsService());

        tokenServices.setUserService(userDetailsService);

        list.add(new RefreshTokenGranter
                (tokenServices,
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory()));
        //自定义账号密码client模式、
        list.add(new MyTokenGranter(
                grantType,
                authenticationManager,
                endpoints.getTokenServices(),
                endpoints.getClientDetailsService(),
                endpoints.getOAuth2RequestFactory()));

        return new CompositeTokenGranter(list);
    }

    /**
     * 配置授权（authorization）以及令牌（token）的访问端点和令牌服务(token services)
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .tokenStore(myRedisTokenStoreService)
                .tokenGranter(tokenGranter(endpoints))
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
                .reuseRefreshTokens(true)
        ;
    }


    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {

        RestAuthenticationEntryPoint authenticationEntryPoint = new RestAuthenticationEntryPoint();
        ClientCredentialsTokenEndpointFilter filter = new ClientCredentialsTokenEndpointFilter();
        filter.setAuthenticationManager(authenticationManager);
        filter.setAuthenticationEntryPoint(authenticationEntryPoint);
        filter.afterPropertiesSet();
        security.addTokenEndpointAuthenticationFilter(filter);
        security
                // 允许表单登录 需要注释掉
                .allowFormAuthenticationForClients()
                // 密码加密编码器
                .passwordEncoder(passwordEncoder)
                // 允许所有的checkToken请求
                .checkTokenAccess("permitAll()");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        if (enabledJdbc) {
            clients.withClientDetails(myClientDetailsService);
        } else {
            clients.inMemory()
                    .withClient(clientId)
                    .secret(StringUtils.isNotEmpty(clientSecret) ? passwordEncoder.encode(clientSecret) : null)
                    .resourceIds("gmwframework")
                    .scopes("all")
                    .authorizedGrantTypes(grantType, "refresh_token")
                    .accessTokenValiditySeconds(accessTokenExpire) //4小时
                    .refreshTokenValiditySeconds(refreshTokenExpire) //6小时
            ;
        }
    }
}
