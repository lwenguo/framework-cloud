package org.gmwframework.cloud.auth.base;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.exception.ApiException;
import org.gmwframework.cloud.common.utils.StrUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 获取当前授权用户信息
 *
 * @author guomw
 * @date 2022/11/05 19:50
 * @since 1.0.0
 */
@Service
@Slf4j
public class AuthHelper {
    public static final String HEADER = "Authorization";
    public static final String TOKEN_KEY = "access_token";
    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";
    @Resource
    private MyRedisTokenStoreService myRedisTokenStoreService;

    private ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    /**
     * 获取request
     */
    public HttpServletRequest getRequest() {
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取客户端请求携带的token
     */
    private String getToken(HttpServletRequest request) {
        String token = request.getParameter(TOKEN_KEY);
        token = StrUtils.isEmpty(token) ? request.getHeader(HEADER) : token;
        if (StrUtils.isNotEmpty(token) && token.startsWith(TOKEN_PREFIX)) {
            token = token.replace(TOKEN_PREFIX, "");
        }
        if (StrUtils.isNotEmpty(token) && token.startsWith(TOKEN_PREFIX.toLowerCase())) {
            token = token.replace(TOKEN_PREFIX.toLowerCase(), "");
        }
        if (StrUtils.isEmpty(token) || "Bearer".equals(token)) {
            throw new ApiException(CommonCode.LACK_TOKEN);
        }
        return token;
    }

    /**
     * 刷新token,并返回用户信息
     */
    public LoginUser refreshToken() {
        String token = getToken(getRequest());
        if (StringUtils.isNotBlank(token)) {
            OAuth2AccessToken accessToken = myRedisTokenStoreService.readAccessToken(token);
            return getLoginUser(accessToken);
        }
        throw new ApiException(CommonCode.TOKEN_EXPIRED);
    }

    /**
     * 退出accessToken
     */
    public void logout() {
        String token = getToken(getRequest());
        if (StringUtils.isNotBlank(token)) {
            OAuth2AccessToken accessToken = myRedisTokenStoreService.readAccessToken(token);
            myRedisTokenStoreService.removeRefreshToken(accessToken.getRefreshToken());
            myRedisTokenStoreService.removeAccessToken(accessToken);
        }
    }

    /**
     * 刷新token
     *
     * @return 返回最新token
     */
    public OAuth2AccessToken refreshToken(OAuth2AccessToken token) {
        OAuth2Authentication oAuth2Authentication = myRedisTokenStoreService.readAuthentication(token);
        return myRedisTokenStoreService.getAccessToken(oAuth2Authentication);
    }

    /**
     * 当前用户
     *
     * @return 当前用户信息
     */
    public LoginUser currentUser() {
        return currentUser(getRequest());
    }

    public LoginUser currentUser(HttpServletRequest request) {
        try {
            String token = getToken(request);
            OAuth2AccessToken oAuth2AccessToken = myRedisTokenStoreService.readAccessToken(token);
            return getLoginUser(oAuth2AccessToken);
        } catch (Exception e) {
            throw new ApiException(CommonCode.TOKEN_EXPIRED);
        }
    }

    private LoginUser getLoginUser(OAuth2AccessToken oAuth2AccessToken) {
        if (oAuth2AccessToken != null && !oAuth2AccessToken.isExpired()) {
            OAuth2Authentication auth2Authentication = myRedisTokenStoreService.readAuthentication(oAuth2AccessToken.getValue());
            UserDetails userDetails = (UserDetails) auth2Authentication.getUserAuthentication().getPrincipal();
            //获取用户信息
            LoginUser details = new LoginUser();
            details.setUserNo(userDetails.getUserNo());
            details.setUserId((Long) userDetails.getId());
            details.setUsername(userDetails.getUsername());
            details.setMobile(userDetails.getMobile());
            details.setSysClient(userDetails.getSysClient());
            details.setDisabled(!userDetails.isEnabled());
            details.setOrgId(userDetails.getOrgId());
            details.setRootOrgId(userDetails.getRootOrgId());
            details.setRoleIds(userDetails.getRoleIds());
            details.setName(userDetails.getName());
            details.setSysUser(userDetails.isSysUser());
            details.setUserType(userDetails.getUserType());
            details.setAvatar(userDetails.getAvatar());
            return details;
        }
        throw new ApiException(CommonCode.TOKEN_EXPIRED);
    }
}
