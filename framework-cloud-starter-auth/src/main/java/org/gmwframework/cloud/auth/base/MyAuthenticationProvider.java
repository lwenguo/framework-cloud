package org.gmwframework.cloud.auth.base;


import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.exception.ApiException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;


@Setter
@Slf4j
public class MyAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private PasswordEncoder passwordEncoder;

    private MyUserDetailsService bizUserService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authenticationToken) throws AuthenticationException {

        if (!userDetails.isEnabled()) {
            log.error("账户已锁定");
            throw new ApiException(CommonCode.UNAUTHORIZED, "该账户已被禁用");
        }

        //密码校验
        String presentedPassword = userDetails.getPassword();
        if (!passwordEncoder.matches((String) authenticationToken.getCredentials(), presentedPassword)) {
            log.error("密码错误");
            throw new ApiException(CommonCode.UNAUTHORIZED, "账号或密码错误");
        }
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        UserDetails userDetails;
        try {
            userDetails = bizUserService.loadUserByUsername(username);
        } catch (Exception e) {
            log.error("账号不存在");
            throw new ApiException(CommonCode.UNAUTHORIZED, "账号或密码错误");
        }

        if (userDetails == null) {
            log.error("账号不存在");
            throw new ApiException(CommonCode.UNAUTHORIZED, "账号或密码错误");
        }
        if (!userDetails.isEnabled()) {
            log.error("账户已锁定");
            throw new ApiException(CommonCode.UNAUTHORIZED, "该账户已被禁用");
        }
        return userDetails;
    }
}
