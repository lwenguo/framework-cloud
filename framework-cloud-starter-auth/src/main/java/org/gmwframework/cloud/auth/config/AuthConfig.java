package org.gmwframework.cloud.auth.config;

import org.gmwframework.cloud.auth.base.MyAuthenticationKeyGenerator;
import org.gmwframework.cloud.auth.base.MyRedisTokenStoreService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
public class AuthConfig {
    @Resource
    private RedisConnectionFactory redisConnectionFactory;
    @Resource
    private ClientDetailsService clientDetailsService;
    @Resource
    private DataSource dataSource;

    @Value("${gmwframework.oauth.one-account:false}")
    private boolean oneAccount;

    /**
     * 密码编码器
     * <p>
     * 委托方式，根据密码的前缀选择对应的encoder，例如：{bcypt}前缀->标识BCYPT算法加密；{noop}->标识不使用任何加密即明文的方式
     * 密码判读 DaoAuthenticationProvider#additionalAuthenticationChecks
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public MyRedisTokenStoreService myRedisTokenStoreService() {
        MyRedisTokenStoreService store = new MyRedisTokenStoreService(redisConnectionFactory, clientDetailsService);
        if (!oneAccount) {
            store.setAuthenticationKeyGenerator(new MyAuthenticationKeyGenerator());
        }
        store.setPrefix("gmwframework:oauth");
        return store;
    }


    @Bean(name = "myClientDetailsService")
    public ClientDetailsService myClientDetailsService() {
        return new JdbcClientDetailsService(dataSource);
    }

}
