package org.gmwframework.cloud.weixin.model;

import lombok.Builder;
import lombok.Data;

/**
 * 微信XML消息实体
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 11:56 上午
 */
@Data
@Builder
public class WxXmlMessageResult {
    /**
     * 消息类型，文本为text
     */
    private String MsgType;
    /**
     * 发送方帐号（一个OpenID）
     */
    private String FromUserName;
    /**
     * 开发者微信号
     */
    private String ToUserName;
    /**
     * 文本消息内容
     */
    private String Content;
    /**
     * 消息id，64位整型
     */
    private Long MsgId;
    /**
     * 消息创建时间 （整型）
     */
    private Long CreateTime;

    private String Event;
    /**
     * 事件key
     */
    private String EventKey;
}
