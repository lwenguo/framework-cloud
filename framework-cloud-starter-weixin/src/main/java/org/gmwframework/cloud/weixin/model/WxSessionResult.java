package org.gmwframework.cloud.weixin.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 2:41 下午
 */
@Data
public class WxSessionResult implements Serializable {
    private static final long serialVersionUID = -1060216618475607933L;

    private String sessionKey;

    private String openid;

    private String unionid;
}
