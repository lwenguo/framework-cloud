package org.gmwframework.cloud.weixin.mapstruct;

import cn.binarywang.wx.miniapp.bean.Watermark;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.gmwframework.cloud.weixin.model.WxPhoneNumberResult;
import org.gmwframework.cloud.weixin.model.WxSessionResult;
import org.gmwframework.cloud.weixin.model.WxUserResult;
import org.gmwframework.cloud.weixin.model.WxWatermark;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 2:34 下午
 */
@Mapper
public interface WxResultMapStruct {
    static WxResultMapStruct getInstance() {
        return WxUserMapStructInstance.INSTANCE;
    }

    final class WxUserMapStructInstance {
        private static final WxResultMapStruct INSTANCE = Mappers.getMapper(WxResultMapStruct.class);

        private WxUserMapStructInstance() {
        }
    }


    WxUserResult wxMpUser2wxUserInfo(WxMpUser wxMpUser);


    WxSessionResult wxMpSession2wxSession(WxMaJscode2SessionResult result);


    WxWatermark watermark2wxWatermark(Watermark watermark);

    @Mappings({
            @Mapping(target = "nickname", source = "nickName"),
            @Mapping(target = "headImgUrl", source = "avatarUrl"),
            @Mapping(target = "sex", source = "gender"),
    })
    WxUserResult wxMaUserInfo2wxUserResult(WxMaUserInfo wxMaUserInfo);

    WxPhoneNumberResult wxMaPhoneNumberInfo2wxPhoneNumberResult(WxMaPhoneNumberInfo wxMaPhoneNumberInfo);
}
