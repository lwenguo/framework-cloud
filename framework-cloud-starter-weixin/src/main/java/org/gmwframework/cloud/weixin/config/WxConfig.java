package org.gmwframework.cloud.weixin.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.WxMaConfig;
import cn.binarywang.wx.miniapp.config.impl.WxMaRedisConfigImpl;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpRedisConfigImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;

/**
 * @author guomw
 * @date 2022/11/08 00:41
 * @since 1.0.0
 */
@Configuration
public class WxConfig {

    @Resource
    private WxProperties wxProperties;

    @Resource
    private JedisPool jedisPool;

    //获取公众号服务实例
    @Bean
    public WxMpService wxMpService() {
        //获取公众号服务实例
        WxMpService _wxMpService = new WxMpServiceImpl();
        WxMpRedisConfigImpl wxMpRedisConfig = new WxMpRedisConfigImpl(jedisPool);
        wxMpRedisConfig.setAppId(wxProperties.getAppId());
        wxMpRedisConfig.setSecret(wxProperties.getAppSecret());
        wxMpRedisConfig.setAesKey(wxProperties.getAesKey());
        wxMpRedisConfig.setToken(wxProperties.getToken());
        WxMpConfigStorage wxMpConfigStorage = wxMpRedisConfig;
        _wxMpService.setWxMpConfigStorage(wxMpConfigStorage);
        return _wxMpService;
    }

    //获取小程序服务实例
    @Bean
    public WxMaService wxMaService() {
        //获取小程序服务实例
        WxMaService _wxMaService = new WxMaServiceImpl();
        WxMaRedisConfigImpl wxMaRedisConfig = new WxMaRedisConfigImpl(jedisPool);
        wxMaRedisConfig.setAppid(wxProperties.getMiniAppId());
        wxMaRedisConfig.setSecret(wxProperties.getMiniAppSecret());
        wxMaRedisConfig.setMsgDataFormat("JSON");
        wxMaRedisConfig.setAesKey(wxProperties.getAesKey());
        wxMaRedisConfig.setToken(wxProperties.getToken());
        WxMaConfig wxMaConfig = wxMaRedisConfig;
        _wxMaService.setWxMaConfig(wxMaConfig);
        return _wxMaService;
    }
}
