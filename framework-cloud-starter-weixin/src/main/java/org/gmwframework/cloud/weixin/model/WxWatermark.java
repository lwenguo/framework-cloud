package org.gmwframework.cloud.weixin.model;

import lombok.Data;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 3:00 下午
 */
@Data
public class WxWatermark {

    private String timestamp;
    private String appid;
}
