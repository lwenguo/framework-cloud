package org.gmwframework.cloud.weixin.model;

import lombok.Data;
import org.gmwframework.cloud.common.utils.StrUtils;

import java.util.List;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 12:04 下午
 */
@Data
public class WxMenuResult {

    /**
     * <pre>
     * 菜单的响应动作类型.
     * view表示网页类型，
     * click表示点击类型，
     * miniprogram表示小程序类型
     * </pre>
     */
    private String type = "view";
    /**
     * <pre>
     * 菜单KEY值，用于消息接口推送，不超过128字节.
     * click等点击类型必须
     * </pre>
     */
    private String key = "view";

    /**
     * 菜单标题，不超过16个字节，子菜单不超过60个字节.
     */
    private String name;

    private String url;
    /**
     * 小程序的页面路径.
     * miniprogram类型必须
     */
    private String pagePath = "pages/index/index";

    /**
     * 子菜单
     */
    private List<WxMenuResult> subMenus;


    public String getUrl() {
        if (StrUtils.isEmpty(url)) {
            return pagePath;
        }
        return url;
    }
}
