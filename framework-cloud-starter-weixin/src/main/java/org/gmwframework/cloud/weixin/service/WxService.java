package org.gmwframework.cloud.weixin.service;

import org.gmwframework.cloud.weixin.model.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 微信常用的接口
 * 这里只是对微信相关操作接口做了个聚合，集中处理
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 11:33 上午
 */
public interface WxService {


    /**
     * 检查微信公众号开发者回调签名
     *
     */
    String checkSignature(HttpServletRequest request);

    /**
     * 微信公众号消息处理
     *
     * @return 返回微信XML消息实体
     */
    WxXmlMessageResult messageHandle(HttpServletRequest request);

    /**
     * <pre>
     * 自定义菜单创建接口
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
     * 如果要创建个性化菜单，请设置matchrule属性
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @return 如果是个性化菜单，则返回menuid，否则返回null
     */
    String menuCreate(List<WxMenuResult> menuModels);

    /**
     * 用oauth2获取用户信息, 当前面引导授权时的scope是snsapi_userinfo的时候才可以.
     *
     * @param code 授权Code
     */
    WxUserResult oauth2getUserInfo(String code);

    /**
     * 获取登录后的session信息.
     *
     * @param code 登录时获取的 code
     */
    WxSessionResult getSessionInfo(String code);

    /**
     * 解密用户敏感数据.
     *
     * @param sessionKey    会话密钥
     * @param encryptedData 消息密文
     * @param ivStr         加密算法的初始向量
     */
    WxUserResult getUserInfo(String sessionKey, String encryptedData, String ivStr);

    /**
     * 解密用户手机号信息.
     *
     * @param sessionKey    会话密钥
     * @param encryptedData 消息密文
     * @param ivStr         加密算法的初始向量
     */
    WxPhoneNumberResult getPhoneNumber(String sessionKey, String encryptedData, String ivStr);
}
