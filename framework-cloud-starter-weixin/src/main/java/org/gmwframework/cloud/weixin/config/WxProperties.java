package org.gmwframework.cloud.weixin.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 微信公众号和小程序相关配置
 */
@Data
@Configuration
public class WxProperties {
    /**
     * 小程序appId
     */
    @Value("${gmwframework.wechat.mini_app_id:}")
    private String miniAppId;
    /**
     * 小程序密钥
     */
    @Value("${gmwframework.wechat.mini_app_secret:}")
    private String miniAppSecret;
    /**
     * 微信公众号 AppId
     */
    @Value("${gmwframework.wechat.app_id:}")
    private String appId;
    /**
     * 微信公众号 secret
     */
    @Value("${gmwframework.wechat.app_secret:}")
    private String appSecret;

    /**
     * 微信公众号 EncodingAESKey 消息加解密密钥
     */
    @Value("${gmwframework.wechat.aes_key:}")
    private String aesKey;
    /**
     * 微信公众号 令牌(Token)
     */
    @Value("${gmwframework.wechat.token:}")
    private String token;
}
