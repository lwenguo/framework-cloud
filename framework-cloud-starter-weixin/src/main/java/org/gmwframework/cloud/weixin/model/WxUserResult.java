package org.gmwframework.cloud.weixin.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 2:32 下午
 */
@Data
public class WxUserResult {

    private Boolean subscribe;
    private String openId;
    private String nickname;
    /**
     * 性别表示：1，2等数字.
     */
    private String sex;
    private String language;
    private String city;
    private String province;
    private String country;
    private String headImgUrl;
    private Long subscribeTime;
    /**
     * https://mp.weixin.qq.com/cgi-bin/announce?action=getannouncement&announce_id=11513156443eZYea&version=&lang=zh_CN
     * <pre>
     * 只有在将公众号绑定到微信开放平台帐号后，才会出现该字段。
     * 另外，在用户未关注公众号时，将不返回用户unionID信息。
     * 已关注的用户，开发者可使用“获取用户基本信息接口”获取unionID；
     * 未关注用户，开发者可使用“微信授权登录接口”并将scope参数设置为snsapi_userinfo，获取用户unionID
     * </pre>
     */
    private String unionId;
    private String remark;

    private WxWatermark watermark;

    public String getSexDesc() {
        String v = "未知";
        if (StringUtils.isNotBlank(sex)) {
            switch (sex) {
                case "1":
                    v = "男";
                    break;
                case "2":
                    v = "女";
                    break;
                default:
                    break;
            }
        }
        return v;
    }
}
