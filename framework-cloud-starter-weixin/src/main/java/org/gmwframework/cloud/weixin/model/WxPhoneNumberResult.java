package org.gmwframework.cloud.weixin.model;

import lombok.Data;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 2:32 下午
 */
@Data
public class WxPhoneNumberResult {

    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;

    private WxWatermark watermark;
}
