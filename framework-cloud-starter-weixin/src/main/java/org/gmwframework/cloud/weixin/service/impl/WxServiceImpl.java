package org.gmwframework.cloud.weixin.service.impl;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMenuService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.gmwframework.cloud.common.utils.StrUtils;
import org.gmwframework.cloud.weixin.config.WxProperties;
import org.gmwframework.cloud.weixin.mapstruct.WxResultMapStruct;
import org.gmwframework.cloud.weixin.model.*;
import org.gmwframework.cloud.weixin.service.WxService;
import org.gmwframework.cloud.weixin.utils.ShaUtil;
import org.gmwframework.cloud.weixin.utils.WxMessageUtil;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 微信常用的接口
 * 这里只是对微信相关操作接口做了个聚合，集中处理
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 11:39 上午
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class WxServiceImpl implements WxService {
    private final WxMpService wxMpService;
    private final WxMaService wxMaService;
    private final WxProperties wxProperties;

    /**
     * 微信公众号开发者回调签名检查
     *
     */
    @Override
    public String checkSignature(HttpServletRequest request) {
        // 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
        String signature = request.getParameter("signature");
        // 时间戳
        String timestamp = request.getParameter("timestamp");
        // 随机数
        String nonce = request.getParameter("nonce");
        // 随机字符串
        String echoStr = request.getParameter("echostr");
        String tmpStr = ShaUtil.getSHA1(wxProperties.getToken(), timestamp, nonce);
        if (tmpStr.equals(signature.toUpperCase())) {
            return echoStr;
        }
        return null;
    }

    /**
     * 微信公众号消息处理
     *
     */
    @Override
    public WxXmlMessageResult messageHandle(HttpServletRequest request) {
        Map<String, String> resultMap = WxMessageUtil.parseXml(request);
        return WxXmlMessageResult.builder()
                .ToUserName(resultMap.get("ToUserName"))
                .FromUserName(resultMap.get("FromUserName"))
                .Content(resultMap.get("Content"))
                .MsgType(resultMap.get("MsgType"))
                .Event(resultMap.get("Event"))
                .EventKey(resultMap.get("EventKey"))
                .build();
    }

    /**
     * <pre>
     * 自定义菜单创建接口
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013&token=&lang=zh_CN
     * 如果要创建个性化菜单，请设置matchrule属性
     * 详情请见：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296&token=&lang=zh_CN
     * </pre>
     *
     * @return 如果是个性化菜单，则返回menuid，否则返回null
     */
    @Override
    public String menuCreate(List<WxMenuResult> menuModels) {
        WxMpMenuService wxMpMenuService = wxMpService.getMenuService();
        WxMenu wxMenu = new WxMenu();
        wxMenu.setButtons(getButtons(menuModels));
        try {
            wxMpMenuService.menuCreate(wxMenu);
            return "ok";
        } catch (Exception e) {
            log.error("menuCreate:" + e.getMessage());
        }
        return null;
    }

    /**
     * 用oauth2获取用户信息, 当前面引导授权时的scope是snsapi_userinfo的时候才可以.
     *
     */
    @Override
    public WxUserResult oauth2getUserInfo(String code) {
        try {
            //用code换取oauth2的access token.
            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            if (wxMpOAuth2AccessToken != null) {
                //用oauth2获取用户信息, 当前面引导授权时的scope是snsapi_userinfo的时候才可以.
                WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, "zh_CN");
                return WxResultMapStruct.getInstance().wxMpUser2wxUserInfo(wxMpUser);
            }
        } catch (WxErrorException e) {
            log.error("oauth2getUserInfo", e);
        }
        return null;
    }

    /**
     * 获取登录后的session信息.
     *
     * @param code 登录时获取的 code
     * @return .
     */
    @Override
    public WxSessionResult getSessionInfo(String code) {
        try {
            WxMaJscode2SessionResult sessionResult = wxMaService.getUserService().getSessionInfo(code);
            if (sessionResult != null) {
                return WxResultMapStruct.getInstance().wxMpSession2wxSession(sessionResult);
            }
        } catch (WxErrorException e) {
            log.error("getSessionInfo", e);
        }
        return null;
    }

    /**
     * 解密用户敏感数据.
     *
     * @param sessionKey    会话密钥
     * @param encryptedData 消息密文
     * @param ivStr         加密算法的初始向量
     */
    @Override
    public WxUserResult getUserInfo(String sessionKey, String encryptedData, String ivStr) {
        WxMaUserInfo wxMaUserInfo = wxMaService
                .getUserService()
                .getUserInfo(sessionKey, encryptedData, ivStr);
        if (wxMaUserInfo != null) {
            return WxResultMapStruct.getInstance().wxMaUserInfo2wxUserResult(wxMaUserInfo);
        }
        return null;
    }

    /**
     * 解密用户手机号信息.
     *
     * @param sessionKey    会话密钥
     * @param encryptedData 消息密文
     * @param ivStr         加密算法的初始向量
     * @return .
     */
    @Override
    public WxPhoneNumberResult getPhoneNumber(String sessionKey, String encryptedData, String ivStr) {
        WxMaPhoneNumberInfo wxMaPhoneNumberInfo = wxMaService
                .getUserService().getPhoneNoInfo(sessionKey, encryptedData, ivStr);
        if (wxMaPhoneNumberInfo != null) {
            return WxResultMapStruct.getInstance().wxMaPhoneNumberInfo2wxPhoneNumberResult(wxMaPhoneNumberInfo);
        }
        return null;
    }


    private List<WxMenuButton> getButtons(List<WxMenuResult> menuModels) {
        List<WxMenuButton> buttons = new ArrayList<>();
        for (WxMenuResult menuModel : menuModels) {
            WxMenuButton button = new WxMenuButton();
            button.setUrl(menuModel.getUrl());
            button.setType(menuModel.getType());
            button.setName(menuModel.getName());
            button.setAppId(wxProperties.getMiniAppId());
            button.setPagePath(menuModel.getPagePath());
            button.setKey(menuModel.getKey());
            if (StrUtils.isCollectNotNull(menuModel.getSubMenus())) {
                button.setSubButtons(getButtons(menuModel.getSubMenus()));
            }
            buttons.add(button);
        }
        return buttons;
    }

}
