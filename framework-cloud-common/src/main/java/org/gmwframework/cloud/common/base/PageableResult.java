package org.gmwframework.cloud.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 分页返回结果基类
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PageableResult<T> {

    @ApiModelProperty("总页数")
    private long pages;
    @ApiModelProperty("页码")
    private long current = 1;
    @ApiModelProperty("每页数量")
    private long size;

    @ApiModelProperty("总数量")
    protected long total;

    @ApiModelProperty("结果")
    protected List<T> records;


    public long getTotal() {
        if (total == 0 && records != null) {
            return records.size();
        }
        return total;
    }

    /**
     * 换个马甲
     *
     * @param func 转换器
     * @param <U>  新的元素乐星
     * @return 新的分页结果
     */
    public <U> PageableResult<U> map(Function<? super T, ? extends U> func) {
        return PageableResult.<U>builder().total(total).size(size).current(current).pages(pages)
                .records(records.stream().map(func).collect(Collectors.toList())).build();
    }
}
