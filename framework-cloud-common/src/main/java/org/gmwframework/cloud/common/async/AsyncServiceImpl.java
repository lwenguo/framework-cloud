/**
 * @(#)AsyncServiceImpl.java, 2022/6/11.
 * <p/>
 * Copyright 2022 Pinheng, Inc. All rights reserved.
 * Pinheng PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.gmwframework.cloud.common.async;

import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;

/**
 * @author wangchen
 * @version 1.0
 * @since 2022/6/11 2022
 */
@Service
@Slf4j
public class AsyncServiceImpl implements AsyncService {
    @Resource(name = "asyncServiceExecutor")
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    @Async("asyncServiceExecutor")
    public void executeAsync(AsyncJob jobFunc) {
        log.debug("Start to execute...");
        jobFunc.doJob();
        log.debug("Executor stop...");
    }

    @Override
    public void execute(AsyncJob func) {
        executeAsync(func);
    }

    @Override
    public void executeAsync(AsyncJob func,LocalDateTime startTime) {
        threadPoolTaskExecutor.execute(() -> {
            try {
                long timestamp = startTime.toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
                ThreadUtil.sleep(timestamp-System.currentTimeMillis(),TimeUnit.MILLISECONDS);
                log.debug("Start to asyncServiceScheduler...");
                func.doJob();
                log.debug("asyncServiceScheduler stop...");
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        });
    }
}
