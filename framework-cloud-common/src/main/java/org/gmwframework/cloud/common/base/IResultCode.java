package org.gmwframework.cloud.common.base;


public interface IResultCode {
    /**
     * 错误结果代码
     *
     */
    String getCode();


    String getMsg();
}
