
package org.gmwframework.cloud.common.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = CodeDescEnumJsonDeserializer.class)
public interface ICommonEnum {
    /**
     * 得到枚举的值
     *
     */
    Object getCode();

    /**
     * 得到枚举值的描述
     *
     */
    Object getDesc();

}
