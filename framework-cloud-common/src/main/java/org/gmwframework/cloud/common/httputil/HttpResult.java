
package org.gmwframework.cloud.common.httputil;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @author guomw
 * @date 2019-10-19
 */
@Data
@AllArgsConstructor
@ToString
public class HttpResult {
    private int httpStatus;
    private String httpContent;
}
