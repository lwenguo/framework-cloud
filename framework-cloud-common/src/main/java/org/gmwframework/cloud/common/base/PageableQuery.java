package org.gmwframework.cloud.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * 分页请求基类
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/18 9:31 上午
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageableQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 最小起始页
     */
    private static final long MIN_PAGE = 1L;

    /**
     * 最大页数
     */
    private static final long MAX_PAGE = 5000L;

    /**
     * 最小页面大小
     */
    private static final long MIN_SIZE = 10L;

    /**
     * 最大页面大小
     */
    private static final long MAX_SIZE = 200L;

    /**
     * 关键字
     */
    @ApiModelProperty("搜索关键字")
    private String keyword;

    @ApiModelProperty("页面记录数")
    @Min(MIN_SIZE)
    @Max(MAX_SIZE)
    private long size;

    @ApiModelProperty("页码")
    @Min(MIN_PAGE)
    @Max(MAX_PAGE)
    private long current;
}
