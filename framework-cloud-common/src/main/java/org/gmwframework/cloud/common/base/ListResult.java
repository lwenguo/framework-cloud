package org.gmwframework.cloud.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 分页返回列表结果
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/18 9:37 上午
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("总数量")
    protected long total;

    @ApiModelProperty("结果")
    protected List<T> records;


    public long getTotal() {
        if (total == 0 && records != null) {
            return records.size();
        }
        return total;
    }
}
