package org.gmwframework.cloud.common.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 结果类型枚举
 */
@AllArgsConstructor
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResultTypeEnum implements ICommonEnum {
    SUCCESS(200, "成功"),
    EXCEPTION(500, "异常"),
    ERROR(501, "错误"),
    FAILED(502, "失败");

    private Integer code;
    private String desc;
}
