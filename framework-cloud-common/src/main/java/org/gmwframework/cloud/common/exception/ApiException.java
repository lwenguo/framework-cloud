package org.gmwframework.cloud.common.exception;

import lombok.Getter;
import org.gmwframework.cloud.common.base.ApiResult;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.base.IResultCode;

/**
 * @date 2019-08-16
 */
@Getter
public class ApiException extends RuntimeException {
    private ApiResult<Void> apiResult;

    public ApiException(String message, ApiResult<Void> apiResult) {
        super(message);
        this.apiResult = apiResult;
    }

    public ApiException(ApiResult<Void> apiResult) {
        super(apiResult.getErrMsg());
        this.apiResult = apiResult;
    }

    public ApiException(String message) {
        super(message);
        this.apiResult = ApiResult.exception(CommonCode.SYSTEM_EXCEPTION, message);
    }

    public ApiException(IResultCode resultCode) {
        super(resultCode.getMsg());
        this.apiResult = ApiResult.exception(resultCode);
    }

    public ApiException(IResultCode resultCode, String message) {
        super(message);
        this.apiResult = ApiResult.exception(resultCode, message);
    }
}
