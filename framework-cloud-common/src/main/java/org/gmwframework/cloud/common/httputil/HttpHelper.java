package org.gmwframework.cloud.common.httputil;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import java.util.List;
import java.util.Map;

/**
 * 网络请求
 *
 * @author guomw
 */
@Slf4j
public class HttpHelper {

    /**
     * post 请求
     */
    public static <T> T post(Class<T> cls, String url, Map<String, String> requestMap, Map<String, String> header) throws Exception {
        return post(cls, url, requestMap, header, null);
    }

    public static <T> T post(Class<T> cls, String url, Map<String, String> requestMap, Map<String, String> header, HttpConfig httpConfig) throws Exception {
        HttpResult httpResult = HttpClientUtil.post(url, requestMap, header, httpConfig);
        if (httpResult.getHttpStatus() == HttpStatus.SC_OK) {
            return JSONObject.parseObject(httpResult.getHttpContent(), cls);
        }
        throw new Exception(httpResult.getHttpContent());
    }

    /**
     * post
     */
    public static <T> T post(Class<T> cls, String url, Map<String, String> requestMap) throws Exception {
        return post(cls, url, requestMap, null, null);
    }

    public static <T> T post(Class<T> cls, String url, Map<String, String> requestMap, HttpConfig config) throws Exception {
        return post(cls, url, requestMap, null, config);
    }


    /**
     * post 请求
     **/
    public static <T> T post(Class<T> cls, String url, String requestData, Map<String, String> header) throws Exception {
        HttpResult httpResult = HttpClientUtil.post(url, requestData, header, null);
        if (httpResult.getHttpStatus() == HttpStatus.SC_OK) {
            return JSONObject.parseObject(httpResult.getHttpContent(), cls);
        }
        throw new Exception(httpResult.getHttpContent());
    }

    /**
     * post
     *
     */
    public static <T> T post(Class<T> cls, String url, String requestData) throws Exception {
        return post(cls, url, requestData, null);
    }

    public static <T> T get(Class<T> cls, String url, Map<String, String> requestMap, Map<String, String> header, HttpConfig httpConfig) throws Exception {
        HttpResult httpResult = HttpClientUtil.get(url, requestMap, header, httpConfig);
        if (httpResult.getHttpStatus() == HttpStatus.SC_OK) {
            return JSONObject.parseObject(httpResult.getHttpContent(), cls);
        }
        throw new Exception(httpResult.getHttpContent());
    }

    public static <T> T get(Class<T> cls, String url) throws Exception {
        return get(cls, url, null, null, null);
    }

    public static <T> List<T> getList(Class<T> cls, String url) throws Exception {
        HttpResult httpResult = HttpClientUtil.get(url, null);
        if (httpResult.getHttpStatus() == HttpStatus.SC_OK) {
            return JSONObject.parseArray(httpResult.getHttpContent(), cls);
        }
        throw new Exception(httpResult.getHttpContent());
    }

    public static <T> T get(Class<T> cls, String url, Map<String, String> requestMap) throws Exception {
        return get(cls, url, requestMap, null);
    }

    public static <T> T get(Class<T> cls, String url, Map<String, String> requestMap, HttpConfig httpConfig) throws Exception {
        return get(cls, url, requestMap, null, httpConfig);
    }

}
