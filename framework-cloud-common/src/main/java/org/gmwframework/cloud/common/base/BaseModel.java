package org.gmwframework.cloud.common.base;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;


@Data
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
public abstract class BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
}
