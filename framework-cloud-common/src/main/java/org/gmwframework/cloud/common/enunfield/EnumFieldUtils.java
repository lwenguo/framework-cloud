package org.gmwframework.cloud.common.enunfield;

import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.common.annotation.EnumField;
import org.gmwframework.cloud.common.base.ICommonEnum;

/**
 * @author guomw
 * @date 2022/07/14 12:40
 * @since 1.0.0
 */
public class EnumFieldUtils {
    /**
     * 根据业务枚举类名和枚举值，获取text
     *
     * @param enumName  枚举路径名称
     * @param enumValue 枚举值
     */
    private static String getEnumValue(String enumName, String enumValue) throws ClassNotFoundException {
        Class<?> cls = Class.forName(enumName);
        Enum[] objs = (Enum[]) cls.getEnumConstants();
        for (int i = 0; i < objs.length; i++) {
            ICommonEnum baseEnum = (ICommonEnum) objs[i];
            if (String.valueOf(baseEnum.getCode().toString()).equals(enumValue)) {
                return baseEnum.getDesc().toString();
            }
        }
        return null;
    }


    /**
     * <p> 获取枚举内容 </p>
     *
     * @return text
     */
    public static String getEnumValue(EnumField enumField, String enumValue) throws ClassNotFoundException {
        String messageSourceKey = getEnumValue(enumField.value(), enumValue);
        String defaultValue = enumField.defaultValue();
        return StringUtils.isNotBlank(messageSourceKey) ? messageSourceKey : defaultValue;
    }
}
