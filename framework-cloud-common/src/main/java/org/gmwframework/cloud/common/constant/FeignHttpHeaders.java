package org.gmwframework.cloud.common.constant;


public final class FeignHttpHeaders {

    /**
     * Feign 调用头
     */
    public static final String FEIGN = "Feign";

    /**
     * 调用跟踪链路ID
     */
    public static final String X_TRACE_ID = "XTraceId";
    /**
     * 用户KEY
     */
    public static final String HEADER_USER = "GF_User";
}
