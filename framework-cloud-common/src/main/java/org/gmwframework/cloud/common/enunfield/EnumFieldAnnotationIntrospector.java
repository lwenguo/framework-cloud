package org.gmwframework.cloud.common.enunfield;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.gmwframework.cloud.common.annotation.EnumField;

/**
 * 注解监视器
 */
public class EnumFieldAnnotationIntrospector extends JacksonAnnotationIntrospector {

    private EnumFieldSerializer enumFieldSerializer;

    public EnumFieldSerializer getEnumFieldSerializer() {
        return enumFieldSerializer;
    }

    public void setLocalizedFieldSerializer(EnumFieldSerializer enumFieldSerializer) {
        this.enumFieldSerializer = enumFieldSerializer;
    }

    @Override
    public Version version() {
        return VersionUtil.versionFor(getClass());
    }


    @Override
    public Object findSerializer(Annotated a) {
        if (a.hasAnnotation(EnumField.class)) {
            if (this.enumFieldSerializer != null) {
                return this.enumFieldSerializer;
            }
        }
        return super.findSerializer(a);
    }
}
