package org.gmwframework.cloud.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 在项目的输出实体字段上加下 @EnumField 注解，可实现将枚举对应的描述信息输出到前端
 * 使用方法：@EnumField(value="你的枚举包名+枚举对象名")，注意，使用的枚举必须是继承 ICommonEnum
 * 如：
 * * @EnumField(value="com.xxxx.xxx.StatusEnum")
 * * private Integer status;
 * * 接口输出字段：
 * * {
 * *    status:0
 * *    statusText:"枚举文字描述"
 * * }
 * </p>
 *
 * @author guomw
 * @date 2022/07/14 12:33
 * @since 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface EnumField {
    /**
     * 对应的Key
     */
    String value();

    /**
     * 如果找不到对应的key的值，采用默认的值（慎用）
     */
    String defaultValue() default "";
}
