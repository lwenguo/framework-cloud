package org.gmwframework.cloud.common.httputil;

import lombok.Data;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 4:11 下午
 */
@Data
public class HttpConfig {
    /**
     * 代理IP
     */
    private String proxyIp;
    /**
     * 代理端口
     */
    private Integer proxyPort;
    /**
     *
     */
    private int connectionRequestTimeout = 300000;

    private int connectTimeout = 300000;

    private int socketTimeout = 300000;
}
