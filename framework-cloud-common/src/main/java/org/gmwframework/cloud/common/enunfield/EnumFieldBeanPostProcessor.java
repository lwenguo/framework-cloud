package org.gmwframework.cloud.common.enunfield;

import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.MessageSource;

/**
 * 自动添加枚举序列化器
 */
public class EnumFieldBeanPostProcessor implements BeanPostProcessor {

    private AnnotationIntrospector annotationIntrospector;

    public AnnotationIntrospector getAnnotationIntrospector() {
        return annotationIntrospector;
    }

    public SerializeFilter serializeFilter;

    public SerializeFilter getSerializeFilter() {
        return serializeFilter;
    }

    public void setSerializeFilter(SerializeFilter serializeFilter) {
        this.serializeFilter = serializeFilter;
    }

    public void setAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this.annotationIntrospector = annotationIntrospector;
    }

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        if (o instanceof ObjectMapper) {
            if (this.getAnnotationIntrospector() != null) {
                ((ObjectMapper) o).setAnnotationIntrospector(this.getAnnotationIntrospector());
            }
        } else if (o instanceof MessageSource) {

        } else if (o instanceof FastJsonHttpMessageConverter) {
//            if (this.getSerializeFilter() != null) {
//                ((FastJsonHttpMessageConverter) o).getFastJsonConfig().setSerializeFilters(this.getSerializeFilter());
//            }
        }
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        return o;
    }
}
