package org.gmwframework.cloud.common.utils;

import java.util.Random;
import java.util.UUID;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/8/11 10:22 上午
 */
public class RandomUtils {
    public static final Random random = new Random(UUID.randomUUID().hashCode());
    private static final String codeStr = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789abcdefghijklmnpqrstuvwxyz";


    /**
     * 根据位数生成随机码
     *
     * @param length 位数
     * @author guomw
     */
    public static String randomString(int length) {
        StringBuilder retNum = new StringBuilder();
        for (int i = 0; i < length; i++) {
            retNum.append(codeStr.charAt(random.nextInt(codeStr.length())));
        }
        return retNum.toString();
    }


    /**
     * 产生在区间内的随机正整数，[min,max]
     *
     * @param min 最小数
     * @param max 最大数
     * @return 产生的随机数
     */
    public static int nextIntInSection(int min, int max) {
        if (min > max) {
            throw new IllegalStateException("最小数不能超过最大数");
        }
        return random.nextInt(max - min + 1) + min;
    }

}
