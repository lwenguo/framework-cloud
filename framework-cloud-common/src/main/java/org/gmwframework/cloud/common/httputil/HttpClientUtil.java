
package org.gmwframework.cloud.common.httputil;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.gmwframework.cloud.common.utils.StrUtils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author guomw
 * @date 2019-10-19
 */
@Slf4j
@SuppressWarnings("unchecked")
public class HttpClientUtil {
    final static String ENCODING_UTF8 = "utf-8";

    private static CloseableHttpClient createHttpClient() {
        return createHttpClient(null);
    }

    private static CloseableHttpClient createHttpClient(HttpConfig httpConfig) {
        if (httpConfig == null) {
            httpConfig = new HttpConfig();
        }
        RequestConfig.Builder builder = RequestConfig.custom()
                .setConnectionRequestTimeout(httpConfig.getConnectionRequestTimeout())
                .setConnectTimeout(httpConfig.getConnectTimeout())
                .setSocketTimeout(httpConfig.getSocketTimeout());
        if (StrUtils.isNotEmpty(httpConfig.getProxyIp()) && httpConfig.getProxyPort() != null) {
            builder.setProxy(new HttpHost(httpConfig.getProxyIp(), httpConfig.getProxyPort()));
        }
        return HttpClientBuilder.create().setDefaultRequestConfig(builder.build()).build();

    }

    public static HttpResult post(String url, Map<String, String> requestMap, HttpConfig httpConfig) {
        return post(url, requestMap, null, httpConfig);
    }

    public static HttpResult post(String url, Map<String, String> requestMap, Map<String, String> header, HttpConfig httpConfig) {
        try (CloseableHttpClient httpClient = createHttpClient(httpConfig)) {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            HttpPost httpPost = new HttpPost(url);
            if (header != null) {
                header.forEach(httpPost::addHeader);
            }
            requestMap.forEach((key, value) -> {
                if (value != null) {
                    nameValuePairs.add(new BasicNameValuePair(key, value));
                }
            });

            HttpEntity httpEntity = new UrlEncodedFormEntity(nameValuePairs, ENCODING_UTF8);
            httpPost.setEntity(httpEntity);
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                return new HttpResult(response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity()));
            }

        } catch (IOException e) {
            return new HttpResult(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    public static HttpResult post(String url, String requestData, HttpConfig httpConfig) {
        return post(url, requestData, null, httpConfig);
    }

    public static HttpResult post(String url, String requestData, Map<String, String> header, HttpConfig httpConfig) {
        try (CloseableHttpClient httpClient = createHttpClient(httpConfig)) {
            StringEntity stringEntity = new StringEntity(requestData, "utf-8");
            HttpPost httpPost = new HttpPost(url);
            if (header != null) {
                header.forEach(httpPost::addHeader);
            }
            httpPost.setEntity(stringEntity);
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                return new HttpResult(response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity()));
            }
        } catch (IOException e) {
            return new HttpResult(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }

    }

    /**
     * 执行一个get请求
     *
     * @param url        请求地址
     * @param requestMap 请求参数
     */
    public static HttpResult get(String url, Map<String, String> requestMap, Map<String, String> header, HttpConfig httpConfig) {
        try (CloseableHttpClient httpClient = createHttpClient(httpConfig)) {
            StringBuilder finalUrl = new StringBuilder(url);
            if (requestMap != null) {
                Iterator iterator = requestMap.entrySet().iterator();
                int index = 0;
                while (iterator.hasNext()) {
                    Map.Entry<String, String> entry = (Map.Entry<String, String>) iterator.next();
                    if (entry.getValue() != null) {
                        if (index == 0) {
                            finalUrl.append("?").append(entry.getKey()).append("=").append(entry.getValue());
                        } else {
                            finalUrl.append("&")
                                    .append(entry.getKey()).append("=")
                                    .append(URLEncoder
                                            .encode(String.valueOf(entry.getValue()), ENCODING_UTF8));
                        }
                    }
                    index++;
                }
            }
            HttpGet httpGet = new HttpGet(finalUrl.toString());
            if (header != null) {
                header.forEach(httpGet::addHeader);
            }

            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                return new HttpResult(response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity()));
            }
        } catch (IOException e) {
            return new HttpResult(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    public static HttpResult get(String url, HttpConfig httpConfig) {
        return get(url, null, null, httpConfig);
    }

    public static HttpResult get(String url, Map<String, String> requestMap, HttpConfig httpConfig) {
        return get(url, requestMap, null, httpConfig);
    }
}
