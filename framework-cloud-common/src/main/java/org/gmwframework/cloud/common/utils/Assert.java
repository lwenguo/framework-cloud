package org.gmwframework.cloud.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.common.exception.ApiException;

/**
 * <p><p>
 */
public class Assert {


    /**
     * 断言是否为真，如果为 {@code false} 抛出 {@code MyException} 异常<br>
     *
     * <pre class="code">
     * Assert.isTrue(i &gt; 0, "The value must be greater than zero");
     * </pre>
     *
     * @param expression 波尔值
     * @param errorMsg   错误抛出异常附带的消息模板
     */
    public static void isTrue(boolean expression, String errorMsg) {
        if (!expression) {
            throw new ApiException(errorMsg);
        }
    }

    /**
     * 断言是否为假，如果为 {@code true} 抛出 {@code MyException} 异常<br>
     *
     * <pre class="code">
     * Assert.isFalse(i &lt; 0, "The value must be greater than zero");
     * </pre>
     *
     * @param expression 波尔值
     * @param errorMsg   错误抛出异常附带的消息模板
     */
    public static void isFalse(boolean expression, String errorMsg) {
        if (expression) {
            throw new ApiException(errorMsg);
        }
    }

    /**
     * 断言对象是否为{@code null} ，如果不为{@code null} 抛出{@link IllegalArgumentException} 异常
     *
     * <pre class="code">
     * Assert.isNull(value, "The value must be null");
     * </pre>
     *
     * @param object   被检查的对象
     * @param errorMsg 消息模板
     */
    public static void isNull(Object object, String errorMsg) {
        if (object == null) {
            throw new ApiException(errorMsg);
        }
    }

    // ----------------------------------------------------------------------------------------------------------- Check empty

    /**
     * 检查给定字符串是否为空，为空抛出 {@link ApiException}
     *
     * <pre class="code">
     * Assert.notEmpty(name, "Name must not be empty");
     * </pre>
     *
     * @param text     被检查字符串
     * @param errorMsg 错误消息模板
     * @return 非空字符串
     */
    public static String notEmpty(String text, String errorMsg) {
        if (StringUtils.isEmpty(text)) {
            throw new ApiException(errorMsg);
        }
        return StringUtils.trim(text);
    }

    /**
     * 检查给定字符串是否为空白（null、空串或只包含空白符），为空抛出 {@link ApiException}
     *
     * <pre class="code">
     * Assert.notBlank(name, "Name must not be blank");
     * </pre>
     *
     * @param text     被检查字符串
     * @param errorMsg 错误消息模板
     * @return 非空字符串
     */
    public static String notBlank(String text, String errorMsg) {
        if (StringUtils.isBlank(text)) {
            throw new ApiException(errorMsg);
        }
        return StringUtils.trim(text);
    }


}
