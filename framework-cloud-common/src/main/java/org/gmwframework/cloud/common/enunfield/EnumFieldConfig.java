package org.gmwframework.cloud.common.enunfield;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author guomw
 * @date 2022/07/14 13:33
 * @since 1.0.0
 */
@Configuration
@ConditionalOnProperty(name = "spring.enum-field.enabled", havingValue = "true")
public class EnumFieldConfig {
    /**
     * 定义序列化器
     *
     */
    @Bean
    public EnumFieldSerializer enumFieldSerializer() {
        return new EnumFieldSerializer();
    }

    /**
     * 定义注解监视器
     *
     */
    @Bean
    public EnumFieldAnnotationIntrospector enumFieldAnnotationIntrospector() {
        EnumFieldAnnotationIntrospector enumFieldAnnotationIntrospector = new EnumFieldAnnotationIntrospector();
        enumFieldAnnotationIntrospector.setLocalizedFieldSerializer(enumFieldSerializer());
        return enumFieldAnnotationIntrospector;
    }

    /**
     * 定义bean后置处理器
     *
     */
    @Bean
    public EnumFieldBeanPostProcessor enumFieldBeanPostProcessor() {
        EnumFieldBeanPostProcessor enumFieldBeanPostProcessor = new EnumFieldBeanPostProcessor();
        enumFieldBeanPostProcessor.setAnnotationIntrospector(enumFieldAnnotationIntrospector());
        return enumFieldBeanPostProcessor;
    }

}
