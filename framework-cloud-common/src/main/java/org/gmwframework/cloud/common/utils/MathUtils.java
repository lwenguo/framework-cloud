package org.gmwframework.cloud.common.utils;

import java.math.BigDecimal;

/**
 * 比较大小
 *
 * @date 2019-10-18
 */
public class MathUtils {
    /**
     * a>b
     *
     */
    public static boolean greater(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) > 0;
    }

    /**
     * a<b
     *
     */
    public static boolean less(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) < 0;
    }

    /**
     * a==b
     *
     */
    public static boolean equalTo(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) == 0;
    }

    /**
     * a>=b
     *
     */
    public static boolean greaterAndEqual(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) > -1;
    }

    /**
     * a<=b
     *
     */
    public static boolean lessAndEqual(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) < 1;
    }
}
