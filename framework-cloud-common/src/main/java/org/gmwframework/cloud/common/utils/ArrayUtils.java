package org.gmwframework.cloud.common.utils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * array 工具
 *
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/4/30 3:55 下午
 */
@SuppressWarnings("unchecked")
public class ArrayUtils {
    /**
     * List<T> 自定义对象，根据某一列利用lambad去重
     *
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
//        Map<Object, Boolean> map = new ConcurrentHashMap<>();
//        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
        return distinctByKeys(keyExtractor);
    }

    /**
     * 根据多列进行排序
     *
     */
    public static <T> Predicate<T> distinctByKeys(Function<? super T, Object>... keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> {
            String key = "";
            for (Function<? super T, Object> objectFunction : keyExtractor) {
                key += objectFunction.apply(t).toString();
            }
            return map.putIfAbsent(key, Boolean.TRUE) == null;
        };
    }

    /**
     * 随机取一条对象
     *
     */
    public static <T> T randomOneObject(List<T> list) {
        if (list.size() > 1) {
            int n = ThreadLocalRandom.current().nextInt(list.size());
            return list.get(n);
        }
        return list.get(0);
    }

    /**
     * 获取总分页数
     *
     */
    public static int getPageCount(List<?> list, int pageSize) {
        if (StrUtils.isCollectNull(list)) {
            return 0;
        }
        int count = list.size(); // 记录总数
        int pageCount = 0; // 页数
        if (count % pageSize == 0) {
            pageCount = count / pageSize;
        } else {
            pageCount = count / pageSize + 1;
        }
        return pageCount;
    }

    /**
     * 对list 对象进行分页获取
     *
     */
    public static <T> List<T> startPage(List<T> list, int pageNum, int pageSize) {
        int pageCount = getPageCount(list, pageSize); // 页数
        if (pageCount == 0 || pageNum > pageCount) {
            return null;
        }
        int fromIndex; // 开始索引
        int toIndex; // 结束索引
        if (pageNum < pageCount) {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = fromIndex + pageSize;
        } else {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = list.size();
        }
        return list.subList(fromIndex, toIndex);
    }

}
