package org.gmwframework.cloud.common.utils;


import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2021/5/7 5:32 下午
 */
public class DesUtil {
    private final static String DES = "DES";
    private final static String ENCODE = "UTF-8";
    private final static String defaultKey = "Aed$wYc#23b7AMcL";


    /**
     * 使用 默认key 加密
     *
     * @return String
     * @date 2019-2-26 下午02:46:43
     */
    public static String encrypt(String data, String secret) throws Exception {
        if (StrUtils.isEmpty(secret)) {
            secret = defaultKey;
        }
        byte[] bt = getByte(data.getBytes(ENCODE), secret.getBytes(ENCODE), Cipher.ENCRYPT_MODE);
        return Base64.getEncoder().encodeToString(bt);
    }

    /**
     * 使用 默认key 解密
     *
     * @return String
     * @date 2015-3-17 下午02:49:52
     */
    public static String decrypt(String data, String secret) throws Exception {
        if (data == null)
            return null;
        if (StrUtils.isEmpty(secret)) {
            secret = defaultKey;
        }
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] buf = decoder.decode(data);
        byte[] bt = getByte(buf, secret.getBytes(ENCODE), Cipher.DECRYPT_MODE);
        return new String(bt, ENCODE);
    }

    /**
     * @param data
     * @param key  加密键byte数组
     */
    private static byte[] getByte(byte[] data, byte[] key, int mode) throws Exception {
        // 生成一个可信任的随机数源
        SecureRandom sr = new SecureRandom();

        // 从原始密钥数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);

        // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey secureKey = keyFactory.generateSecret(dks);

        // Cipher对象实际完成加密解密操作
        Cipher cipher = Cipher.getInstance(DES);

        // 用密钥初始化Cipher对象
        cipher.init(mode, secureKey, sr);

        return cipher.doFinal(data);
    }

}
