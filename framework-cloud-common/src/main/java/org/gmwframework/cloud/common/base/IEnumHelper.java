package org.gmwframework.cloud.common.base;

/**
 * 对于枚举的工具类
 *
 * @author allan
 */
@SuppressWarnings("unchecked")
public class IEnumHelper {

    /**
     * 根据枚举值得到枚举描述
     *
     */
    public static Object getEnumDescByCode(Class<? extends ICommonEnum> cls, Object code) {
        ICommonEnum ice = getEnumTypeByCode(cls, code);
        if (ice != null) {
            return ice.getDesc();
        }
        return null;
    }

    /**
     * 根据枚举值得到枚举类型
     *
     */
    public static <T extends ICommonEnum> T getEnumTypeByCode(Class<?> cls, Object code) {
        if (code == null) {
            return null;
        }
        for (Object enumConstant : cls.getEnumConstants()) {
            ICommonEnum item = (ICommonEnum) enumConstant;
            if (item.getCode().equals(code)) {
                return (T) item;
            }
        }
        return null;
    }

    public static <T extends ICommonEnum> T getEnumTypeByCode(Class<?> cls, Object code, T defaultVal) {
        if (code == null) {
            return defaultVal;
        }
        for (Object enumConstant : cls.getEnumConstants()) {
            ICommonEnum item = (ICommonEnum) enumConstant;
            if (item.getCode().equals(code)) {
                return (T) item;
            }
        }
        return defaultVal;
    }
}
