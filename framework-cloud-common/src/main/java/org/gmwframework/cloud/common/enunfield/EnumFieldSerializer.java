package org.gmwframework.cloud.common.enunfield;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.gmwframework.cloud.common.annotation.EnumField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * @author guomw
 * @date 2022/07/14 12:39
 * @since 1.0.0
 */
public class EnumFieldSerializer extends JsonSerializer<Object> {
    private final Logger logger = LoggerFactory.getLogger(EnumFieldSerializer.class);

    private final static String OBJ_SUFFIX = "Text";

    //匹配的类型
    private Set<Class<?>> typeSet = new HashSet<>();

    public Set<Class<?>> getTypeSet() {
        return typeSet;
    }

    public void setTypeSet(Set<Class<?>> typeSet) {
        this.typeSet = typeSet;
    }

    {
        //目前只支持整型或者字符串类型
        typeSet.add(int.class);
        typeSet.add(Integer.class);
        typeSet.add(String.class);
    }

    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        //这里是正常输出
        jsonGenerator.writeObject(o);
        try {
            //扩展输出，出错了也暂时不管
            String filedName = jsonGenerator.getOutputContext().getCurrentName();
            Field field = ReflectionUtils.findField(jsonGenerator.getCurrentValue().getClass(), filedName);
            if (field != null) {
                EnumField enumField = AnnotationUtils.findAnnotation(field, EnumField.class);
                if (enumField != null) {
                    if (this.checkType(field.getType())) {
                        String localeValue = EnumFieldUtils.getEnumValue(enumField, String.valueOf(o));
                        jsonGenerator.writeStringField(jsonGenerator.getOutputContext().getCurrentName() + OBJ_SUFFIX
                                , localeValue);
                    } else {
                        logger.warn("the type:{} of field:{} in class {} ", field.getType().getClass().getName(),
                                filedName, o.getClass().getName());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Jackson serialize object error:", e);
        }
    }

    /**
     * 判断是否在其中
     *
     */
    private boolean checkType(Class<?> type) {
        return typeSet.contains(type);
    }
}
