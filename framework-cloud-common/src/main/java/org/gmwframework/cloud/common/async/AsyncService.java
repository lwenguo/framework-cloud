/**
 * @(#)AsyncService.java, 2022/6/11.
 * <p/>
 * Copyright 2022 Pinheng, Inc. All rights reserved.
 * Pinheng PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.gmwframework.cloud.common.async;

import java.time.LocalDateTime;

/**
 * @author wangchen
 * @version 1.0
 * @since 2022/6/11 2022
 */
public interface AsyncService {
    /**
     * 执行异步任务的方法
     * 
     * @param jobFunc
     *            任务函数
     */
    void executeAsync(AsyncJob jobFunc);

    void execute(AsyncJob func);

    /**
     * 异步执行 desc
     *
     * @param func
     *            func 执行的方法
     * @param startTime
     *            startTime 开始时间的时间，必须大于当前时间
     * @author Guomw 2023/5/26 15:44
     * @since 1.0.0
     */
    void executeAsync(AsyncJob func, LocalDateTime startTime);

    interface AsyncJob {
        /**
         * 异步执行任务
         */
        void doJob();
    }
}
