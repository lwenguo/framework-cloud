package org.gmwframework.cloud.common.utils;


import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;

/**
 * @author guomw (guomwchen@foxmail.com)
 * @date 2020/11/16 1:28 下午
 */
public class IdUtils {
    /**
     * 获取随机UUID
     *
     * @return 随机UUID
     */
    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线
     *
     * @return 简化的UUID，去掉了横线
     */
    public static String simpleUUID() {
        return UUID.randomUUID().toString(true);
    }

    /**
     * 获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID
     *
     * @return 随机UUID
     */
    public static String fastUUID() {
        return UUID.fastUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线，使用性能更好的ThreadLocalRandom生成UUID
     *
     * @return 简化的UUID，去掉了横线
     */
    public static String fastSimpleUUID() {
        return UUID.fastUUID().toString(true);
    }

    /**
     * 简化的UUID，去掉了横线
     *
     * @param secret 生成密钥
     */
    public static String simpleUUID(String secret) {
        try {
            return DigestUtils.md5DigestAsHex((fastUUID() + secret).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return simpleUUID();
    }
}
