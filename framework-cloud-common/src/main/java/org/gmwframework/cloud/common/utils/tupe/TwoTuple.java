package org.gmwframework.cloud.common.utils.tupe;

/**
 * 二元组
 */
public class TwoTuple<A, B> {
    public final A first;
    public final B second;

    public TwoTuple(A a, B b) {
        this.first = a;
        this.second = b;
    }
}