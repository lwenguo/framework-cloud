/**
 * @(#)ExecutorConfig.java, 2022/6/11.
 * <p/>
 * Copyright 2022 Pinheng, Inc. All rights reserved.
 * Pinheng PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.gmwframework.cloud.common.async;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author wangchen
 * @version 1.0
 * @since 2022/6/11 2022
 */
@Configuration
@EnableAsync
@Slf4j
public class ThreadExecutorConfig {
    @Value("${async.executor.thread.core_pool_size:5}")
    private int corePoolSize = 5;

    @Value("${async.executor.thread.max_pool_size:20}")
    private int maxPoolSize = 20;

    @Value("${async.executor.thread.queue_capacity:1000}")
    private int queueCapacity = 1000;

    @Value("${async.executor.thread.name.prefix:}")
    private String namePrefix = "";

    @Bean(name = "asyncServiceExecutor")
    public Executor asyncServiceExecutor() {
        log.info("Start physical thread executor...");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //配置核心线程数
        executor.setCorePoolSize(corePoolSize);
        //配置最大线程数
        executor.setMaxPoolSize(maxPoolSize);
        //配置队列大小
        executor.setQueueCapacity(queueCapacity);
        //配置线程池中的线程的名称前缀
        executor.setThreadNamePrefix(namePrefix);

        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //执行初始化
        executor.initialize();
        return executor;
    }
}
