package org.gmwframework.cloud.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.gmwframework.cloud.common.utils.StrUtils;

import java.beans.Transient;
import java.io.Serializable;

/**
 * @author allan
 * @date 2019-08-16
 */
@Data
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = -6109881088423213930L;

    /**
     * 结果类型
     * {@link ResultTypeEnum}
     */
    @ApiModelProperty("成功200，异常500，错误501，失败502")
    private int code;
    /**
     * 错误码
     */
    private String errorCode;
    /**
     * 信息
     */
    private String errMsg;
    /**
     * 封装的数据类型
     */
    private T data;

    private boolean success;

    private String xTraceId;

    /**
     * 成功
     *
     */
    public static <T> ApiResult<T> success(T data) {
        return write(ResultTypeEnum.SUCCESS, CommonCode.OK, null, data);
    }

    public static <T> ApiResult<T> success(T data, String xTraceId) {
        ApiResult<T> apiResult = write(ResultTypeEnum.SUCCESS, CommonCode.OK, null, data);
        apiResult.setXTraceId(xTraceId);
        return apiResult;
    }

    /**
     * 成功
     *
     */
    public static <T> ApiResult<T> success() {
        return write(ResultTypeEnum.SUCCESS, CommonCode.OK, null, null);
    }

    /**
     * 异常结果
     *
     */
    public static <T> ApiResult<T> exception(IResultCode resultCode) {
        return exception(resultCode, null);
    }

    /**
     * 异常结果
     *
     */
    public static <T> ApiResult<T> exception(IResultCode resultCode, String msg) {
        return write(ResultTypeEnum.EXCEPTION, resultCode, msg, null);
    }

    /**
     * 错误结果
     *
     */
    public static <T> ApiResult<T> error(IResultCode resultCode) {
        return error(resultCode, null);
    }

    /**
     * 错误结果
     *
     */
    public static <T> ApiResult<T> error(IResultCode resultCode, String msg) {
        return write(ResultTypeEnum.ERROR, resultCode, msg, null);
    }

    public static <T> ApiResult<T> error(IResultCode resultCode, String msg, String xTraceId) {
        ApiResult<T> apiResult = write(ResultTypeEnum.ERROR, resultCode, msg, null);
        apiResult.setXTraceId(xTraceId);
        return apiResult;
    }


    private static <T> ApiResult<T> write(ResultTypeEnum typeEnum, IResultCode resultCode, String msg, T data) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(typeEnum.getCode());
        if (resultCode != null) {
            apiResult.setErrorCode(resultCode.getCode());
            apiResult.setErrMsg(StrUtils.isEmpty(msg) ? resultCode.getMsg() : msg);
        } else {
            apiResult.setErrMsg(StrUtils.isEmpty(msg) ? typeEnum.getDesc() : msg);
        }
        apiResult.setData(data);
        return apiResult;
    }

    public boolean isSuccess() {
        return this.code == ResultTypeEnum.SUCCESS.getCode();
    }
}
