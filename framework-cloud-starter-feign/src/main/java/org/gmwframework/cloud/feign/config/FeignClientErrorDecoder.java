package org.gmwframework.cloud.feign.config;

import com.alibaba.fastjson.JSON;
import feign.FeignException;
import feign.Response;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.gmwframework.cloud.common.base.ApiResult;
import org.gmwframework.cloud.common.base.CommonCode;
import org.gmwframework.cloud.common.exception.ApiException;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * @version 1.0
 * @date 2022/3/31 1:03
 */
@Slf4j
public class FeignClientErrorDecoder extends ErrorDecoder.Default {
    @Override
    public Exception decode(String methodKey, Response response) {
        Exception exception = super.decode(methodKey, response);

        if (exception instanceof RetryableException) {
            return exception;
        }

        if (exception instanceof FeignException && responseBody(((FeignException) exception).content()).isPresent()) {
            Optional<ByteBuffer> optional = responseBody(((FeignException) exception).content());
            ByteBuffer responseBody = optional.get();
            String bodyText;
            try {
                bodyText = StandardCharsets.UTF_8.newDecoder().decode(responseBody.asReadOnlyBuffer()).toString();
            } catch (CharacterCodingException e) {
                return new ApiException(CommonCode.SYSTEM_EXCEPTION);
            }

            try {
                ApiResult<?> result = JSON.parseObject(bodyText, ApiResult.class);
                String message = Optional.ofNullable(result.getErrorCode()).orElse(CommonCode.SYSTEM_EXCEPTION.getMsg());
                return new ApiException(message);
            } catch (Exception e) {
                // 不是有效的json字串
                return new ApiException(bodyText);
            }
        }
        return new ApiException(CommonCode.SYSTEM_EXCEPTION);
    }

    public Optional<ByteBuffer> responseBody(byte[] responseBody) {
        return responseBody == null ? Optional.empty() : Optional.of(ByteBuffer.wrap(responseBody));
    }
}
