package org.gmwframework.cloud.feign.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.codec.ErrorDecoder;
import org.gmwframework.cloud.common.constant.FeignHttpHeaders;
import org.gmwframework.cloud.common.utils.StrUtils;
import org.slf4j.MDC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;


@Configuration
public class FeignConfiguration implements RequestInterceptor {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new FeignClientErrorDecoder();
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        if (Objects.nonNull(requestAttributes)) {
            // 来自Web的Feign调用 设置成可以继承
            RequestContextHolder.setRequestAttributes(requestAttributes, Boolean.TRUE);

            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
                    .currentRequestAttributes();
            HttpServletRequest request = attributes.getRequest();

            //传递YC_User
            String userKey = request.getHeader(FeignHttpHeaders.HEADER_USER);
            if (StrUtils.isNotBlank(userKey)) {
                requestTemplate.header(FeignHttpHeaders.HEADER_USER, userKey);
            }
            // 传递TraceID调用链路
            String xTraceId = request.getHeader(FeignHttpHeaders.X_TRACE_ID);
            if (StrUtils.isNotBlank(xTraceId)) {
                requestTemplate.header(FeignHttpHeaders.X_TRACE_ID, xTraceId);
            } else {
                requestTemplate.header(FeignHttpHeaders.X_TRACE_ID, getTraceId());
            }
        } else {
            // 来自非Web的Feign调用
            RequestContextHolder.setRequestAttributes(new NonWebRequestAttributes(), Boolean.TRUE);
            requestTemplate.header(FeignHttpHeaders.X_TRACE_ID, getTraceId());
        }
        // 设置Feign标记
        requestTemplate.header(FeignHttpHeaders.FEIGN, "active");
    }

    private String getTraceId() {
        return Optional.ofNullable(MDC.get(FeignHttpHeaders.X_TRACE_ID)).orElseGet(() -> {
            String uuid = UUID.randomUUID().toString();
            MDC.put(FeignHttpHeaders.X_TRACE_ID, uuid);
            return uuid;
        });
    }
}
