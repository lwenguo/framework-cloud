package org.gmwframework.cloud.excel;

import com.alibaba.excel.EasyExcel;
import lombok.NonNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author guomw
 * @date 2022/10/09 13:03
 * @since 1.0.0
 */
@Configuration
public class ExcelWebEnhance implements HandlerInterceptor, WebMvcConfigurer {

    @Override
    public void addReturnValueHandlers(@NonNull List<HandlerMethodReturnValueHandler> handlers) {
        WebMvcConfigurer.super.addReturnValueHandlers(handlers);
        handlers.add(new HandlerMethodReturnValueHandler() {
            @Override
            public boolean supportsReturnType(@NonNull MethodParameter returnType) {
                return returnType.getParameterType() == DownloadableExcel.class;
            }

            @Override
            public void handleReturnValue(Object returnValue, @NonNull MethodParameter returnType
                    , @NonNull ModelAndViewContainer mavContainer, @NonNull NativeWebRequest webRequest) throws Exception {
                HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
                if (response == null)
                    throw new Exception("never");
                mavContainer.setRequestHandled(true);

                if (returnValue == null) {
                    response.sendError(404);
                    return;
                }
                if (returnType.getParameterType() != DownloadableExcel.class) {
                    response.sendError(404);
                    return;
                }

                DownloadableExcel<?> data = (DownloadableExcel<?>) returnValue;
                // https://www.rfc-editor.org/rfc/rfc5987.txt
                response.setHeader("Content-Disposition", "attachment; filename* = UTF-8''" + URLEncoder.encode(data.getName() + ".xls", "UTF-8"));
                response.setContentType("application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                try (OutputStream output = response.getOutputStream()) {
                    EasyExcel.write(output, data.getType())
                            .sheet(data.getName())
                            .doWrite(data.getList());
                    output.flush();
                }

            }
        });
    }

}
