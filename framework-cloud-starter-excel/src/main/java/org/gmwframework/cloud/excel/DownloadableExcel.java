package org.gmwframework.cloud.excel;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * excel97~2003
 * application/vnd.ms-excel
 * application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
 *
 * @author guomw
 * @date 2022/10/09 13:03
 * @since 1.0.0
 */
@Data
@Builder
public class DownloadableExcel<T> {
    /**
     * 文件名，不用带后缀
     */
    private String name;

    /**
     * 列类型
     */
    private Class<T> type;

    /**
     * 数据
     */
    private List<T> list;
}

