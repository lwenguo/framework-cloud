package org.gmwframework.cloud.dlock.config;

import org.gmwframework.cloud.dlock.enums.RedisType;

/**
 * 配置类:redis的分布式锁配置 兼容多种redis类型的配置@seeRedisType
 *
 * @see RedisType
 */
public class RedisConfig extends AbstractDistributedLockConfig {

    /**
     * redis类型
     */
    private RedisType redisType;

    /**
     * master服务器
     *
     * <p>redisType为SENTINEL时为主服务器名,为MASTERSLAVE时为主节点地址</p>
     */
    private String masterServer;

    /**
     * redis服务器
     *
     * <p>redisType为SENTINEL时为哨兵节点地址,CLUSTER是为集群节点地址,为MASTERSLAVE时为从节点地址 </p>
     */
    private String redisServer;

    public String getMasterServer() {
        return masterServer;
    }

    public void setMasterServer(String masterServer) {
        this.masterServer = masterServer;
    }

    public RedisType getRedisType() {
        return redisType;
    }

    public void setRedisType(RedisType redisType) {
        this.redisType = redisType;
    }

    public String getRedisServer() {
        return redisServer;
    }

    public void setRedisServer(String redisServer) {
        this.redisServer = redisServer;
    }

}
