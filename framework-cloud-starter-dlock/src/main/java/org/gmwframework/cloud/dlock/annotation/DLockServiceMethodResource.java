package org.gmwframework.cloud.dlock.annotation;

import com.google.common.base.Preconditions;

import org.gmwframework.cloud.dlock.utils.MvelUtils;
import org.gmwframework.cloud.dlock.utils.ObjectUtils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 分布式锁服务的方法资源
 */
public class DLockServiceMethodResource {

    /**
     * 分布式锁拦截的方法参数
     */
    private final Object[] args;

    /**
     * 分布式锁拦截的方法资源
     */
    private final Method method;

    /**
     * 真实的锁名
     */
    private final String realLockName;

    DLockServiceMethodResource(ProceedingJoinPoint proceedingJoinPoint,
                               DistributedLock distributedLock) {
        Preconditions.checkNotNull(proceedingJoinPoint, "proceedingJoinPoint can not be null");

        Preconditions.checkNotNull(distributedLock, "distributedLock can not be null");

        method = ((MethodSignature) proceedingJoinPoint.getSignature()).getMethod();

        this.args = proceedingJoinPoint.getArgs();

        realLockName = generateRealLockName(distributedLock.lockName());
    }

    private String generateRealLockName(String lockName) {
        String[] parameterNames = ObjectUtils.getParameterNames(method);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        for (int i = 0; i < parameterNames.length; i++) {
            paramMap.put(parameterNames[i], args[i]);
        }
        return MvelUtils.eval(lockName, paramMap);
    }

    /**
     * 返回真实的锁名
     */
    String getRealLockName() {
        return realLockName;
    }

    /**
     * 返回拦截的方法
     */
    Method getMethod() {
        return method;
    }
}
