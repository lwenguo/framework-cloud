package org.gmwframework.cloud.dlock.utils;

import com.google.common.base.Strings;

import org.mvel2.MVEL;

import java.util.Map;

/**
 * 工具类:mevl表达式的工具类
 */
public class MvelUtils {

    /**
     * 解析mvel格式的表达式
     *
     * @param evalExpression mvel表达式
     * @param variableMap    mvel需要的值
     * @return 解析后的值
     */
    public static String eval(String evalExpression, Map<String, Object> variableMap) {
        if (Strings.isNullOrEmpty(evalExpression) || variableMap == null || variableMap.size() == 0)
            return evalExpression;

        return String.valueOf(MVEL.eval(evalExpression, variableMap));
    }
}
