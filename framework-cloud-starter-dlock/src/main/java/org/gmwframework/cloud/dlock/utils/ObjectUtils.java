package org.gmwframework.cloud.dlock.utils;

import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;

import java.lang.reflect.Method;

/**
 * 工具类:对象操作类
 */
public class ObjectUtils {

    private final static ParameterNameDiscoverer PARAMETER_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();

    /**
     * 获取方法中的所有参数名
     *
     * @param method 方法
     */
    public static String[] getParameterNames(Method method) {
        if (method == null) {
            return new String[0];
        }

        return PARAMETER_NAME_DISCOVERER.getParameterNames(method);
    }
}
