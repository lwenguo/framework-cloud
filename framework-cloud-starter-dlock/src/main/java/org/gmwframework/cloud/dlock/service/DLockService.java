package org.gmwframework.cloud.dlock.service;

import org.gmwframework.cloud.dlock.config.DistributedLockConfig;
import org.gmwframework.cloud.dlock.exception.LockException;

/**
 * 接口类:锁服务接口类
 */
public interface DLockService {

    /**
     * 初始化分布式锁服务
     *
     * @param distributedLockConfig 分布式锁服务的配置
     */
    void init(DistributedLockConfig distributedLockConfig);

    /**
     * 阻塞方式获取锁
     *
     * @param lockName 锁名
     * @throws LockException 获取锁失败 抛出异常
     */
    void lock(String lockName) throws LockException;

    /**
     * 支持在事务范围内阻塞方式获取锁 <p>如果enableTransactionSupport参数为true且在事务范围内,则伴随事务提交,锁自动提交</p>
     *
     * @param lockName                 锁名
     * @param enableTransactionSupport 是否支持事务提交
     * @throws LockException 获取锁失败 抛出异常
     */
    void lock(String lockName, boolean enableTransactionSupport) throws LockException;

    /**
     * 非阻塞方式获取锁
     *
     * <p>能获取锁马上则返回true,不能获取锁则返回false</p>
     *
     * @param lockName 锁名
     * @return false表示获取锁 true表示没有获取到锁
     */
    boolean tryLock(String lockName);

    /**
     * 在指定时间范围内非阻塞方式获取锁
     *
     * <p>如果enableTransactionSupport参数为true且在事务范围内,则伴随事务提交,锁自动提交</p>
     *
     * @param waitTime                 锁等待时间
     * @param leaseTime                锁占用时间
     * @param enableTransactionSupport 开始事务支持
     * @return false表示获取锁 true表示没有获取到锁
     */
    boolean tryLock(String lockName, long waitTime, long leaseTime, boolean enableTransactionSupport);

    /**
     * 解锁
     *
     * @param lockName 锁名
     * @throws LockException 解锁失败抛出锁异常
     */
    void unLock(String lockName) throws LockException;

    /**
     * 释放同一个重入号的锁,默认为同一线程
     *
     * @param lockName 锁名
     * @throws LockException 解锁失败抛出锁异常
     */
    void fullyUnLock(String lockName) throws LockException;
}
