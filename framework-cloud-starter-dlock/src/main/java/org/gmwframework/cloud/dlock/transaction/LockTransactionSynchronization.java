package org.gmwframework.cloud.dlock.transaction;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;

/**
 * 事务同步器:锁的事务同步器
 */
public class LockTransactionSynchronization extends TransactionSynchronizationAdapter {

    private static final Logger logger = LoggerFactory
            .getLogger(LockTransactionSynchronization.class);

    private final LockResourceHolder lockResourceHolder;

    public LockTransactionSynchronization(LockResourceHolder lockResourceHolder) {
        this.lockResourceHolder = lockResourceHolder;
        Preconditions.checkNotNull(lockResourceHolder, "lock resource can not be null");
    }

    @Override
    public void afterCompletion(int status) {
        if (Strings.isNullOrEmpty(lockResourceHolder.getLockName())) {
            LoggerUtil.warn(logger, "[op:afterCompletion]lock name can not be null");
            return;
        }

        if (lockResourceHolder.getLockService() == null) {
            LoggerUtil.warn(logger, "[op:afterCompletion]lock service can not be null[lockName={0}]",
                    lockResourceHolder.getLockName());
            return;
        }

        lockResourceHolder.getLockService().fullyUnLock(lockResourceHolder.getLockName());

        LoggerUtil.debug(logger, "[op:afterCompletion]release lock in transaction[lockName={0}]",
                lockResourceHolder.getLockName());
    }
}
