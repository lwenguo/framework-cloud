package org.gmwframework.cloud.dlock.exception;

/**
 * 异常类:锁的异常类
 */
public class LockException extends RuntimeException {

    public LockException() {
        super();
    }

    public LockException(String message) {
        super(message);
    }

    public LockException(String message, Throwable cause) {
        super(message, cause);
    }
}
