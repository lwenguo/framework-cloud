package org.gmwframework.cloud.dlock.service.redis;

import org.gmwframework.cloud.dlock.exception.LockException;

/**
 * 实现类: mock分布式锁服务
 */
public class MockRedisDLockService extends AbstractRedisDLockService {

    public boolean doTryLock(String lockName) {
        return true;
    }

    @Override
    protected Object doInitLockResource() {
        return new Object();
    }

    @Override
    protected void doLock(String lockName) {
    }

    @Override
    protected boolean doTryLock(String lockName, long waitTime, long leaseTime) {
        return true;
    }


    @Override
    protected void doFullyUnLock(String lockName) throws LockException {

    }

    @Override
    protected void doUnLock(String lockName) throws LockException {

    }
}
