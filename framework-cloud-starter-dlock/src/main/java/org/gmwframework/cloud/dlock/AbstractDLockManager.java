package org.gmwframework.cloud.dlock;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import com.alibaba.fastjson.JSON;
import org.gmwframework.cloud.dlock.config.DistributedLockConfig;
import org.gmwframework.cloud.dlock.enums.LockServiceType;
import org.gmwframework.cloud.dlock.factory.DLockServiceFactory;
import org.gmwframework.cloud.dlock.service.DLockService;
import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 抽象类:抽象的分布式锁服务
 *
 * <p>代理底层真实的分布式锁服务</p>
 */
abstract class AbstractDLockManager implements DLockManager {

    private final static Logger logger = LoggerFactory.getLogger(AbstractDLockManager.class);

    /**
     * 分布式锁服务的类型
     */
    private LockServiceType lockServiceType = LockServiceType.REDIS;

    /**
     * 分布式锁服务的配置
     */
    private DistributedLockConfig distributedLockConfig;
    /**
     * 真实的分布式锁
     */
    private DLockService dLockService;

    public void setDistributedLockConfig(
            DistributedLockConfig distributedLockConfig) {
        this.distributedLockConfig = distributedLockConfig;
    }

    public void setLockServiceType(LockServiceType lockServiceType) {
        this.lockServiceType = lockServiceType;
    }

    /**
     * 分布式锁管理服务的初始化
     */
    public void init() {

        Preconditions.checkNotNull(distributedLockConfig, "dLock Config can not be null");

        //创建分布式锁服务实例
        Optional<DLockService> dLockServiceOptional = DLockServiceFactory
                .createDLockService(lockServiceType, distributedLockConfig);

        if (!dLockServiceOptional.isPresent()) {

            LoggerUtil.info(logger, "dLock service init fail[lockServiceType={0},distributedLockConfig={1}]",
                    lockServiceType,
                    JSON.toJSONString(distributedLockConfig));

            throw new IllegalArgumentException("dLock service init fail");
        }

        dLockService = dLockServiceOptional.get();
    }

    DLockService getDLockService() {
        if (dLockService == null) {
            throw new IllegalArgumentException("dLock Service can not be null , must invoke AbstractDLockManager.init()");
        }
        return dLockService;
    }
}
