package org.gmwframework.cloud.dlock.config;

/**
 * 配置类:抽象的分布式锁配置
 */
public abstract class AbstractDistributedLockConfig implements DistributedLockConfig {

    /**
     * 重试次数
     */
    private int retryAttempt = 1;

    /**
     * 连接服务的超时时间
     */
    private int timeout = 3000;

    /**
     * 鉴权密码 如果不需要则为空
     */
    private String password;

    public int getRetryAttempt() {
        return retryAttempt;
    }

    public void setRetryAttempt(int retryAttempt) {
        this.retryAttempt = retryAttempt;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
