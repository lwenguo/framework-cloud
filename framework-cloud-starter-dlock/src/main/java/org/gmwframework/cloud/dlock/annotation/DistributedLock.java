package org.gmwframework.cloud.dlock.annotation;

import org.gmwframework.cloud.dlock.enums.LockServiceType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static org.gmwframework.cloud.dlock.constant.LockConstant.DEFAULT_LOCK_RELEASE_TIME;
import static org.gmwframework.cloud.dlock.constant.LockConstant.DEFAULT_LOCK_WAIT_TIME;
import static org.gmwframework.cloud.dlock.enums.LockServiceType.REDIS;

/**
 * 注解类:分布式锁 <p>在方法范围内加锁,锁定方法内的资源</p> Created by hzmawenjun on 17/12/10.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DistributedLock {

    /**
     * 锁服务名 不设置或者只有一种锁服务,则使用默认的锁服务
     */
    LockServiceType serviceType() default REDIS;

    /**
     * 锁的名字 <p>支持mvel表达式标识锁名</p>
     */
    String lockName();

    /**
     * 获取锁的等待时间,单位为ms
     */
    long waitTime() default DEFAULT_LOCK_WAIT_TIME;

    /**
     * 锁的占用时间,单位为ms
     */
    long releaseTime() default DEFAULT_LOCK_RELEASE_TIME;
}
