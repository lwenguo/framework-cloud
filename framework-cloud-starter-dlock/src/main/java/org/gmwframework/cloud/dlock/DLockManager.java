package org.gmwframework.cloud.dlock;

import org.gmwframework.cloud.dlock.exception.LockException;

/**
 * 接口类:分布式锁管理类
 */
public interface DLockManager {

    /**
     * 非阻塞方式获取锁
     *
     * <p>能获取锁马上则返回true,不能获取锁则返回false</p>
     *
     * <p>支持同一个线程重入</p>
     *
     * @param lockName 锁名
     * @return false表示获取锁 true表示没有获取到锁
     */
    boolean tryLock(String lockName);

    /**
     * 在指定时间范围内非阻塞方式获取锁
     *
     *
     * <p>支持同一个线程重入</p>
     *
     * @param lockName 锁名
     * @param waitTime 锁等待时间 单位为ms
     * @return false表示获取锁 true表示没有获取到锁
     */
    boolean tryLock(String lockName, long waitTime);


    /**
     * 在指定时间范围内非阻塞方式获取锁
     *
     * <p> enableTransactionSupport参数为true且不在事务范围内,则抛出IllegalArgumentException
     * <p>
     * 如果enableTransactionSupport参数为true且在事务范围内,则伴随事务提交,自动释放锁 如果为false则需要调用unLock或者超过leaseTime时间来释放锁
     * </p>
     *
     * <p>支持同一个线程重入</p>
     *
     * @param lockName  锁名
     * @param waitTime  锁等待时间 单位为ms
     * @param leaseTime 锁占用时间 单位为ms
     * @return false表示获取锁 true表示没有获取到锁
     */
    boolean tryLock(String lockName, long waitTime, long leaseTime);

    /**
     * 在指定时间范围内非阻塞方式获取锁
     *
     * <p>enableTransactionSupport参数为true且不在事务范围内,则抛出IllegalArgumentException
     * 如果enableTransactionSupport参数为true且在事务范围内,则伴随事务提交,自动释放锁 如果为false则需要调用unLock或者超过leaseTime时间来释放锁
     * </p>
     *
     * <p>支持同一个线程重入</p>
     *
     * @param waitTime                 锁等待时间 单位为ms
     * @param leaseTime                锁占用时间 单位为ms
     * @param enableTransactionSupport 开始事务支持
     * @return false表示获取锁 true表示没有获取到锁
     */
    boolean tryLock(String lockName, long waitTime, long leaseTime, boolean enableTransactionSupport);

    /**
     * 释放锁
     *
     * @param lockName 锁名
     * @throws LockException 解锁失败抛出锁异常
     */
    void unLock(String lockName) throws LockException;
}
