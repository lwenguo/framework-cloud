package org.gmwframework.cloud.dlock.enums;

/**
 * 枚举类:分布式锁服务类型
 */
public enum LockServiceType {
    /**
     * 基于redis实现的分布式服务
     */
    REDIS("redis分布式锁服务"),

    /**
     * 为了解决分布式资源的环境问题,本地可以调试
     */
    MOCK("mock分布式锁服务"),
    ;

    private String desc;

    LockServiceType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
