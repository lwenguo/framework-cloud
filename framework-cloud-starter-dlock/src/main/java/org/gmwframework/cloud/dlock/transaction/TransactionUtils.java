package org.gmwframework.cloud.dlock.transaction;

import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * 工具类:事务相关的工具类
 */
public class TransactionUtils {

    private final static Logger logger = LoggerFactory.getLogger(TransactionUtils.class);

    /**
     * 注册事务同步器
     */
    public static void registerTransactionSynchronisation(LockResourceHolder lockResourceHolder) {
        if (lockResourceHolder == null) {
            LoggerUtil.warn(logger,
                    "[op:registerTransactionSynchronisation]register Synchronisation lock resource can not be null");
            return;
        }

        LoggerUtil.debug(logger,
                "[op:registerTransactionSynchronisation]register TransactionSynchronisation[lockName={0}]",
                lockResourceHolder.getLockName());
        TransactionSynchronizationManager
                .registerSynchronization(new LockTransactionSynchronization(lockResourceHolder));

    }

    /**
     * 判断是否处于有效的事务范围
     *
     * @return false表示不存在事务  true表示存在事务
     */
    public static boolean isActualTransactionActive() {
        return TransactionSynchronizationManager.isActualTransactionActive();
    }
}
