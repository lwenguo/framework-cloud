package org.gmwframework.cloud.dlock.factory;

import com.google.common.base.Optional;

import com.alibaba.fastjson.JSON;
import org.gmwframework.cloud.dlock.config.DistributedLockConfig;
import org.gmwframework.cloud.dlock.enums.LockServiceType;
import org.gmwframework.cloud.dlock.service.DLockService;
import org.gmwframework.cloud.dlock.service.redis.AbstractRedisDLockService;
import org.gmwframework.cloud.dlock.service.redis.MockRedisDLockService;
import org.gmwframework.cloud.dlock.service.redis.RedissonDLockService;
import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 工厂类:锁服务的工程类
 */
public class DLockServiceFactory {

    private final static Logger logger = LoggerFactory.getLogger(DLockServiceFactory.class);

    /**
     * 创建分布式锁服务
     *
     * @param dLockConfig     分布式锁的配置
     * @param lockServiceType 分布式锁服务类型
     */
    public static Optional<DLockService> createDLockService(LockServiceType lockServiceType,
                                                            DistributedLockConfig dLockConfig) {

        if (lockServiceType == null || dLockConfig == null) {
            LoggerUtil.error(logger, "dLockConfig or lockServiceType can not be null");
            return Optional.absent();
        }

        try {
            DLockService dLockService;
            switch (lockServiceType) {
                case REDIS:
                    dLockService = createRedissonDLockService(dLockConfig);
                    return Optional.of(dLockService);

                case MOCK:
                    dLockService = createMockDLockService(dLockConfig);
                    return Optional.of(dLockService);
            }

            LoggerUtil
                    .info(logger, "create dLock service[lockServiceType={0},distributedLockConfig={1}]", lockServiceType,
                            JSON.toJSONString(dLockConfig));

        } catch (Exception e) {
            LoggerUtil.error(logger, "[create dLock service fail[lockServiceType={0},distributedLockConfig={1}]",
                    lockServiceType,
                    JSON.toJSONString(dLockConfig), e);
        }


        return Optional.absent();
    }

    /**
     * 创建redisson实现的分布式锁
     *
     * @param distributedLockConfig 分布式锁的配置
     */
    private static DLockService createRedissonDLockService(
            DistributedLockConfig distributedLockConfig) {
        DLockService dLockService = new RedissonDLockService();
        dLockService.init(distributedLockConfig);
        return dLockService;
    }

    /**
     * 创建mock的分布式锁服务
     *
     * @param distributedLockConfig 分布式锁的配置
     */
    private static DLockService createMockDLockService(DistributedLockConfig distributedLockConfig) {
        AbstractRedisDLockService dLockService = new MockRedisDLockService();
        dLockService.init(distributedLockConfig);
        return dLockService;
    }
}
