package org.gmwframework.cloud.dlock.constant;

/**
 * 常量类:锁的常量类
 */
public class LockConstant {

    /**
     * 默认锁的等待时间,单位为ms
     */
    public static final long DEFAULT_LOCK_WAIT_TIME = 1000;

    /**
     * 默认的锁占用时间,单位为ms
     */
    public static final long DEFAULT_LOCK_RELEASE_TIME = 5 * 1000;
}
