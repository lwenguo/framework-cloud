package org.gmwframework.cloud.dlock.service.redis;

import com.google.common.base.Preconditions;

import com.alibaba.fastjson.JSON;
import org.gmwframework.cloud.dlock.config.RedisConfig;
import org.gmwframework.cloud.dlock.service.AbstractDLockService;
import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 服务类:redis服务的抽象类
 *
 * <p>redis分布式锁支持两种实现方式: 1、传入redisconfig 2、传入redis资源实例 </p>
 */
public abstract class AbstractRedisDLockService extends AbstractDLockService {

    private final static Logger logger = LoggerFactory.getLogger(AbstractRedisDLockService.class);

    /**
     * redis资源的配置
     */
    private RedisConfig redisConfig;

    protected void doInit() {
        LoggerUtil.info(logger, "redis dLock service init start");

        if (getLockResourceObject() != null) {
            LoggerUtil
                    .info(logger, "lock resource object has be injected by bean ref[{0}]", getClass().getSimpleName());
            return;
        }

        Preconditions.checkNotNull(getDistributedLockConfig(), "redis config can not be null");

        if (!(getDistributedLockConfig() instanceof RedisConfig)) {
            LoggerUtil.error(logger, "init redis dLock service must be redis config,but config is {0}",
                    JSON.toJSONString(getDistributedLockConfig()));
            throw new IllegalArgumentException("distributedLockConfig is not redis config");
        }

        redisConfig = (RedisConfig) getDistributedLockConfig();

        //执行初始化
        Object lockResourceObject = doInitLockResource();

        LoggerUtil.info(logger, "redis dLock service init complete");

        setLockResourceObject(lockResourceObject);
    }

    RedisConfig getRedisConfig() {
        return redisConfig;
    }

    /**
     * 执行redis锁服务初始化操作
     *
     * <p>比如redssion分布式锁为RedissonClient</p>
     */
    protected abstract Object doInitLockResource();
}
