package org.gmwframework.cloud.dlock.service.redis;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import org.apache.commons.lang3.StringUtils;
import org.gmwframework.cloud.dlock.config.RedisConfig;
import org.gmwframework.cloud.dlock.enums.RedisType;
import org.gmwframework.cloud.dlock.exception.LockException;
import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.Codec;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * 实现类: redisson分布式锁服务
 */
public final class RedissonDLockService extends AbstractRedisDLockService {

    private final static Logger logger = LoggerFactory.getLogger(RedissonDLockService.class);

    public RedissonDLockService() {
    }

    public RedissonDLockService(RedissonClient redissonClient) {
        setLockResourceObject(redissonClient);
    }


    @Override
    protected void doLock(String lockName) {
        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil.error(logger, "get lock name is null[lockName={0}]", lockName);
            throw new LockException("lockName can not be null");
        }

        try {
            RLock rLock = ((RedissonClient) getLockResourceObject()).getLock(lockName);
            if (rLock == null) {
                LoggerUtil.error(logger, "get lock fail, lock is null[lockName={0}]", lockName);
                throw new LockException(String.format("lock Name %s can not be get lock", lockName));
            }

            rLock.lock();

        } catch (Exception e) {
            LoggerUtil.error(logger, e, "get lock fail[lockName={0}]", lockName);
            throw new LockException(String.format("lock Name %s get lock fail", lockName), e);
        }
    }

    public boolean doTryLock(String lockName) {

        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil.error(logger, "try to get lock ,lock name is null[lockName={0}}]", lockName);
            return false;
        }

        try {
            RLock rLock = ((RedissonClient) getLockResourceObject()).getLock(lockName);
            if (rLock == null) {
                LoggerUtil.error(logger, "try to  get lock but lock is null[lockName={0}]", lockName);
                return false;
            }

            boolean tryLock = rLock.tryLock();
            if (!tryLock) {
                LoggerUtil.error(logger, "try to get lock fail[lockName={0}]", lockName);
                return false;
            }
        } catch (Exception e) {
            LoggerUtil.error(logger, e, "try to get lock fail[lockName={0}]", lockName);
            return false;
        }
        return true;
    }

    @Override
    protected boolean doTryLock(String lockName, long waitTime, long leaseTime) {
        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil
                    .error(logger, "try to get lock but lock name is null[lockName={0},waitTime={1},leaseTime={2}]", lockName, waitTime, leaseTime);
            return false;
        }

        try {
            RLock rLock = ((RedissonClient) getLockResourceObject()).getLock(lockName);
            if (rLock == null) {
                LoggerUtil.error(logger, "try to get lock null[lockName={0},waitTime={1}]", lockName, waitTime);
                return false;
            }

            boolean tryLock = rLock.tryLock(waitTime, leaseTime, TimeUnit.MILLISECONDS);
            if (!tryLock) {
                LoggerUtil.error(logger, "try to get lock fail[lockName={0},waitTime={1},leaseTime={2}]", lockName, waitTime, leaseTime);
                return false;
            }
        } catch (Exception e) {
            LoggerUtil.error(logger, e, "try to get lock fail[lockName={0},waitTime={1},leaseTime={2}]", lockName, waitTime, leaseTime);
            return false;
        }
        return true;
    }

    public void doUnLock(String lockName) {
        if (Strings.isNullOrEmpty(lockName)) {
            throw new LockException("lock name is empty");
        }

        try {
            RLock rLock = ((RedissonClient) getLockResourceObject()).getLock(lockName);

            rLock.unlock();
        } catch (Exception e) {
            LoggerUtil.error(logger, e, "unLock fail[lockName={0}]", lockName);
        }
    }

    public void doFullyUnLock(String lockName) throws LockException {
        if (Strings.isNullOrEmpty(lockName)) {
            throw new LockException("lock name is empty");
        }

        try {
            RLock rLock = ((RedissonClient) getLockResourceObject()).getLock(lockName);

            int lockCount = rLock.getHoldCount();
            for (int i = 0; i < lockCount; i++) {
                rLock.unlock();
            }
        } catch (Exception e) {
            LoggerUtil.error(logger, e, "fullyUnLock fail[lockName={0}]", lockName);
        }
    }

    public Object doInitLockResource() {
        LoggerUtil.info(logger, "init Redisson dLock resource");

        RedisConfig redisConfig = getRedisConfig();

        RedisType redisType = redisConfig.getRedisType();

        Preconditions.checkNotNull(redisType, "redisType can not be null");

        Config config;
        switch (redisType) {
            case SINGLE_SERVER:
                config = createSingleServerConfig(redisConfig);
                break;
            case CLUSTER:
                config = createClusterConfig(redisConfig);
                break;
            case SENTINEL:
                config = createSentinelConfig(redisConfig);
                break;
            case MASTERSLAVE:
                config = createMasterSlaveConfig(redisConfig);
                break;
            default:
                throw new IllegalArgumentException("redisType " + redisType + " is not support");
        }

        return Redisson.create(config);
    }

    /**
     * 创建单服务器模式的redisson配置
     */
    private Config createSingleServerConfig(RedisConfig redisConfig) {
        Codec codec = new StringCodec();

        String redisStr = redisConfig.getRedisServer();
        if (!redisStr.startsWith("redis://") && !redisStr.startsWith("rediss://")) {
            redisStr = "redis://" + redisStr;
        }
        Config config = new Config();
        config.setCodec(codec)
                .useSingleServer()
                .setAddress(redisStr)
                .setTimeout(redisConfig.getTimeout())
                .setRetryAttempts(redisConfig.getRetryAttempt())
                .setPassword(StringUtils.trimToNull(getRedisConfig().getPassword()));
        return config;
    }

    /**
     * 创建MasterSlave模式的redisson配置
     *
     * @param redisConfig redis配置
     */
    private Config createMasterSlaveConfig(RedisConfig redisConfig) {
        Codec codec = new StringCodec();

        String redisStr = redisConfig.getRedisServer();
        String[] redisArr = redisStr.split("[,]");

        Preconditions.checkState(redisArr.length > 0, "redis 地址配置错误[" + redisStr + "]");
        Config config = new Config();
        config.setCodec(codec)
                .useMasterSlaveServers()
                .setMasterAddress(redisConfig.getMasterServer())
                .addSlaveAddress(redisArr)
                .setTimeout(redisConfig.getTimeout())
                .setRetryAttempts(redisConfig.getRetryAttempt())
                .setPassword(StringUtils.trimToNull(getRedisConfig().getPassword()));

        return config;
    }

    /**
     * 创建cluster模式的redisson配置
     *
     * @param redisConfig redis配置
     */
    private Config createClusterConfig(RedisConfig redisConfig) {
        Codec codec = new StringCodec();

        String redisStr = redisConfig.getRedisServer();
        String[] redisArr = redisStr.split("[,]");

        Preconditions.checkState(redisArr.length > 0, "redis 地址配置错误[" + redisStr + "]");
        Config config = new Config();
        config.setCodec(codec)
                .useClusterServers()
                .addNodeAddress(redisArr)
                .setTimeout(redisConfig.getTimeout())
                .setRetryAttempts(redisConfig.getRetryAttempt())
                .setPassword(StringUtils.trimToNull(getRedisConfig().getPassword()));

        return config;
    }

    /**
     * 创建哨兵模式的redisson配置
     */
    private Config createSentinelConfig(RedisConfig redisConfig) {
        Codec codec = new StringCodec();

        String redisStr = redisConfig.getRedisServer();
        String[] redisArr = redisStr.split("[,]");

        Preconditions.checkState(redisArr.length > 0, "redis 地址配置错误[" + redisStr + "]");
        Config config = new Config();
        config.setCodec(codec)
                .useSentinelServers()
                .setMasterName(redisConfig.getMasterServer())
                .addSentinelAddress(redisArr)
                .setTimeout(redisConfig.getTimeout())
                .setRetryAttempts(redisConfig.getRetryAttempt())
                .setPassword(StringUtils.trimToNull(getRedisConfig().getPassword()));
        return config;
    }
}
