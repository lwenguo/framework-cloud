package org.gmwframework.cloud.dlock.annotation;

import com.google.common.base.Preconditions;

import com.alibaba.fastjson.JSON;
import org.gmwframework.cloud.dlock.DLockManager;
import org.gmwframework.cloud.dlock.exception.LockException;
import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * 拦截器: 基于注解的锁服务的拦截器
 *
 * <p>在方法级别上加上注解@DistributedLock</p>
 */
@Aspect
public class DLockServiceMethodInterceptor implements InitializingBean {

    private final static Logger logger = LoggerFactory.getLogger(DLockServiceMethodInterceptor.class);

    /**
     * 分布式锁的管理服务
     */
    private DLockManager dLockManager;

    public void setdLockManager(DLockManager dLockManager) {
        this.dLockManager = dLockManager;
    }

    public void afterPropertiesSet() throws Exception {
        Preconditions.checkNotNull(dLockManager, "simpleDLockManager can not be null");
    }

    /**
     * 拦截加上@DistributedLock注解的请求
     */
    @Pointcut(value = "@annotation(org.gmwframework.cloud.dlock.annotation.DistributedLock)")
    public void distributedLock() {
    }

    @Around(value = "distributedLock() && @annotation(distributedLock)")
    public Object invokeLockService(ProceedingJoinPoint proceedingJoinPoint,
                                    DistributedLock distributedLock)
            throws Throwable {

        //获取真实的realName
        DLockServiceMethodResource dLockServiceMethodResource = new DLockServiceMethodResource(
                proceedingJoinPoint, distributedLock);

        LoggerUtil.debug(logger,
                "dLock service intercept service[methodName={0},lockName={1},realLockName={2}] ",
                dLockServiceMethodResource.getMethod().getName(), distributedLock.lockName(),
                dLockServiceMethodResource.getRealLockName());


        boolean locked = false;
        try {
            //获取锁
            locked = dLockManager
                    .tryLock(dLockServiceMethodResource.getRealLockName(), distributedLock.waitTime(),
                            distributedLock
                                    .releaseTime(), false);

            if (!locked) {

                LoggerUtil.error(logger,
                        "Interceptor dLock Service fail[dLockServiceMethodResource={0}]",
                        JSON.toJSON(dLockServiceMethodResource));

                throw new LockException(
                        String.format("dLock name %s get Lock error",
                                dLockServiceMethodResource.getRealLockName()));
            }
            //执行底层服务
            Object result = proceedingJoinPoint.proceed();

            LoggerUtil.debug(logger,
                    "dLock service intercept service end[serviceName={0},lockName={1}]",
                    distributedLock.serviceType(), distributedLock.lockName());

            return result;
        } finally {
            try {
                if (locked) {
                    //解锁,解锁失败也不影响业务
                    dLockManager.unLock(dLockServiceMethodResource.getRealLockName());
                }
            } catch (Exception e) {
                LoggerUtil.error(logger, e, "dLock service intercept service unlock fail[realLockName={0}]", dLockServiceMethodResource.getRealLockName());
            }
        }
    }
}
