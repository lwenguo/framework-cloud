package org.gmwframework.cloud.dlock.service;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import org.gmwframework.cloud.dlock.config.DistributedLockConfig;
import org.gmwframework.cloud.dlock.exception.LockException;
import org.gmwframework.cloud.dlock.transaction.LockResourceHolder;
import org.gmwframework.cloud.dlock.transaction.TransactionUtils;
import org.gmwframework.cloud.dlock.utils.LoggerUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.gmwframework.cloud.dlock.transaction.TransactionUtils.isActualTransactionActive;

/**
 * 服务类:锁服务的抽象类
 */
public abstract class AbstractDLockService implements DLockService {

    private final static Logger logger = LoggerFactory.getLogger(AbstractDLockService.class);

    /**
     * 分布式锁的配置
     */
    private DistributedLockConfig distributedLockConfig;

    /**
     * 分布式资源实例
     */
    private Object lockResourceObject;

    public void init(DistributedLockConfig distributedLockConfig) {
        this.distributedLockConfig = distributedLockConfig;

        Preconditions.checkNotNull(distributedLockConfig, "dLock config can not be null");

        doInit();
    }

    protected abstract void doInit();

    /**
     * 获取分布式锁的配置
     */
    protected DistributedLockConfig getDistributedLockConfig() {
        return distributedLockConfig;
    }

    protected Object getLockResourceObject() {
        return lockResourceObject;
    }

    public void setLockResourceObject(Object lockResourceObject) {
        this.lockResourceObject = lockResourceObject;
    }

    public void lock(String lockName, boolean enableTransactionSupport) throws LockException {
        LoggerUtil.debug(logger, "get lock[lockName={0}]", lockName);

        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil.error(logger, "lock fail,lock name is null[lockName={0}]", lockName);
            throw new LockException("lockName can not be null");
        }

        if (enableTransactionSupport) {
            if (!isActualTransactionActive()) {
                throw new IllegalArgumentException(
                        String.format("lockName %s current lock is not in transaction", lockName));
            }
            TransactionUtils.registerTransactionSynchronisation(new LockResourceHolder(lockName, this));
        }

        doLock(lockName);
    }

    /**
     * 执行阻塞的分布式锁服务
     *
     * @param lockName 锁名
     */
    protected abstract void doLock(String lockName);

    public boolean tryLock(String lockName, long waitTime, long leaseTime,
                           boolean enableTransactionSupport) {

        LoggerUtil
                .debug(logger, "try to  get lock[lockName={0},waitTime={1},leaseTime={2},enableTransactionSupport={3}]",
                        lockName, waitTime, leaseTime, enableTransactionSupport);

        if (Strings.isNullOrEmpty(lockName) || waitTime < 0 || leaseTime < 0) {
            LoggerUtil
                    .error(logger, "try to get lock fail, lock name is null[lockName={0},waitTime={1},leaseTime={2}]",
                            lockName, waitTime, leaseTime);
            return false;
        }

        if (enableTransactionSupport) {
            if (!isActualTransactionActive()) {
                throw new IllegalArgumentException(
                        String.format("lockName %s current lock is not in transaction", lockName));
            }

            TransactionUtils.registerTransactionSynchronisation(new LockResourceHolder(lockName, this));
        }

        return doTryLock(lockName, waitTime, leaseTime);
    }

    public boolean tryLock(String lockName) {
        LoggerUtil.debug(logger, "try to  get lock[lockName={0}]", lockName);

        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil.error(logger, "try to get lock fail ,lock name is null[lockName={0}}]", lockName);
            return false;
        }

        return doTryLock(lockName);
    }

    /**
     * 非阻塞方式尝试获取锁的操作
     */
    protected abstract boolean doTryLock(String lockName);

    /**
     * 执行尝试获取锁的操作
     */
    protected abstract boolean doTryLock(String lockName, long waitTime, long leaseTime);

    public void lock(String lockName) throws LockException {
        lock(lockName, false);
    }


    public void fullyUnLock(String lockName) throws LockException {
        LoggerUtil.debug(logger, "fully unlock[lockName={0}]", lockName);

        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil.error(logger, "fullyUnLock fail,lock name is null[lockName={0}]", lockName);
            throw new LockException("lockName can not be null");
        }

        doFullyUnLock(lockName);
    }

    protected abstract void doFullyUnLock(String lockName) throws LockException;


    public void unLock(String lockName) throws LockException {
        LoggerUtil.debug(logger, "unLock [lockName={0}]", lockName);

        if (Strings.isNullOrEmpty(lockName)) {
            LoggerUtil.error(logger, "unLock fail,lock name is null[lockName={0}]", lockName);
            throw new LockException("lockName can not be null");
        }

        doUnLock(lockName);

    }

    /**
     * 执行解锁操作
     *
     * @param lockName 锁名
     */
    protected abstract void doUnLock(String lockName) throws LockException;
}
