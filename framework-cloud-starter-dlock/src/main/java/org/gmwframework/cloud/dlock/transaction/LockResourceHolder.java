package org.gmwframework.cloud.dlock.transaction;

import org.gmwframework.cloud.dlock.service.DLockService;

import org.springframework.transaction.support.ResourceHolder;

/**
 * 事务锁资源holders
 */
public final class LockResourceHolder implements ResourceHolder {

    /**
     * 锁名
     */
    private String lockName;

    /**
     * 资源是否解绑
     */
    private boolean unbound;

    /**
     * 锁服务
     */
    private DLockService lockService;

    public LockResourceHolder(String lockName, DLockService lockService) {
        this.lockName = lockName;
        this.lockService = lockService;
    }

    public void reset() {

    }

    public DLockService getLockService() {
        return lockService;
    }

    public String getLockName() {
        return lockName;
    }

    public void unbound() {
        unbound = true;
    }

    public boolean isVoid() {
        return this.unbound;
    }
}
