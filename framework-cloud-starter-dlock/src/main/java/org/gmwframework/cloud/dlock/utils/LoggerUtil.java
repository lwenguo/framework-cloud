package org.gmwframework.cloud.dlock.utils;

import org.slf4j.Logger;

import java.text.MessageFormat;

/**
 * 日志帮助类
 */
public class LoggerUtil {

    /**
     * 线程编号修饰符
     */
    private static final char THREAD_RIGHT_TAG = ']';

    /**
     * 线程编号修饰符
     */
    private static final char THREAD_LEFT_TAG = '[';

    /**
     * warn日志打印
     */
    public static void warn(Logger logger, String message, Object... params) {
        logger.warn(getLogString(message, params));
    }

    /**
     * info日志打印
     */
    public static void info(Logger logger, String message, Object... params) {
        if (logger.isInfoEnabled()) {
            logger.info(getLogString(message, params));
        }
    }

    /**
     * debug日志打印
     */
    public static void debug(Logger logger, String message, Object... params) {
        if (logger.isDebugEnabled()) {
            logger.debug(getLogString(message, params));
        }
    }


    /**
     * error日志打印
     */
    public static void error(Logger logger, String message,
                             Object... params) {
        logger.error(getLogString(message, params));
    }

    /**
     * error日志打印
     */
    public static void error(Logger logger, Throwable e, String message,
                             Object... params) {
        logger.error(getLogString(message, params), e);

    }

    /**
     * 生成输出到日志的字符串
     */
    public static String getLogString(String message, Object... params) {
        try {
            StringBuilder log = new StringBuilder();
            log.append(THREAD_LEFT_TAG).append(Thread.currentThread().getId())
                    .append(THREAD_RIGHT_TAG);
            log.append(formatMessage(message, params));
            return log.toString();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 格式化日志
     */
    private static String formatMessage(String message, Object[] params) {
        return message != null && params != null && params.length != 0
                ? MessageFormat.format(message, params)
                : message;
    }
}
