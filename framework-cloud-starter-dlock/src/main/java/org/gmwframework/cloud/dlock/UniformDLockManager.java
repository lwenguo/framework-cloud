package org.gmwframework.cloud.dlock;

import org.gmwframework.cloud.dlock.constant.LockConstant;
import org.gmwframework.cloud.dlock.exception.LockException;

/**
 * 实现类:统一的分布式锁管理类
 */
public class UniformDLockManager extends AbstractDLockManager {

    public boolean tryLock(String lockName) {
        return getDLockService().tryLock(lockName);
    }

    public boolean tryLock(String lockName, long waitTime, long leaseTime) {
        return getDLockService()
                .tryLock(lockName, waitTime, leaseTime, false);
    }

    public boolean tryLock(String lockName, long waitTime) {
        return getDLockService()
                .tryLock(lockName, waitTime, LockConstant.DEFAULT_LOCK_RELEASE_TIME, false);
    }

    public boolean tryLock(String lockName, long waitTime, long leaseTime,
                           boolean enableTransactionSupport) {
        return getDLockService().tryLock(lockName, waitTime, leaseTime, enableTransactionSupport);
    }

    public void unLock(String lockName) throws LockException {
        getDLockService().unLock(lockName);
    }
}
