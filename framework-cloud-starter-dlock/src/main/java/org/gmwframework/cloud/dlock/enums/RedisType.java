package org.gmwframework.cloud.dlock.enums;

/**
 * 枚举类:redis的类别
 */
public enum RedisType {
    /**
     * 单服务器模式
     */
    SINGLE_SERVER("单服务模式"),

    /**
     * 主从模式
     */
    MASTERSLAVE("主从模式"),

    /**
     * 集群模式
     */
    CLUSTER("集群模式"),

    /**
     * 哨兵模式
     */
    SENTINEL("哨兵模式");

    /**
     * redis类型描述
     */
    String desc;

    RedisType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
